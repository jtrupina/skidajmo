<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table for associating roles to users (Many-to-Many)
        /*
        Schema::create('favorites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('korisnici_id')->nullable();
            $table->integer('software_id')->nullable();
        });

        Schema::table('favorites', function (Blueprint $table) {
            $table->foreign('korisnici_id')->references('id')->on('korisnici')->onDelete('cascade');
            $table->foreign('software_id')->references('id')->on('software')->onDelete('cascade');
        });
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('favorites');
    }
}
