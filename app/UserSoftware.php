<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSoftware extends Model
{
    protected $table = 'user_software';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public $timestamps = false;
}
