<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'kategorije';

    public $timestamps = false;

    protected $fillable = ['imeKategorije', 'nameKategorije', 'imeUrl', 'nameUrl'];

    public function softwares()
    {
        return $this->hasMany('App\Software', 'kategorija');
    }
}
