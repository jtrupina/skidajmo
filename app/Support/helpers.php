<?php

function parseCategoryName($str){
    $str = str_replace("&","and",$str);
    $str = str_replace(" ","_",$str);
    $str = str_replace("__","_",$str);
    $str = str_replace("?„â€???â‚￢???„Ä…?‚Â?","c",$str);
    $str = str_replace("?„â€???â‚￢??Ä‚Ë???â€?Â￢??â€ˇ","c",$str);
    $str = str_replace("Ä‚â€??„â€|Ä‚â€???â‚￢Ë‡","s",$str);
    $str = str_replace("?„â€???â‚￢??Ä‚Ë???â€?Â￢?‚Â?","d",$str);
    $str = str_replace("Ä‚â€??„â€|Ä‚â€??„Ä?","z",$str);
    $str = str_replace("Ä‚â€??„â€|Ä‚â€?Ä?Ä„","Z",$str);
    $str = str_replace("?„â€???â‚￢??Ä‚Ë???â€?Â￢?‚Â?","C",$str);
    $str = str_replace("?„â€???â‚￢???„Ä…Ä?Ë‡","C",$str);
    $str = str_replace("Ä‚â€??„â€|Ä‚â€??‚Â?","S",$str);
    $str = str_replace("?„â€???â‚￢??Ä‚â€??‚Â?","D",$str);

    return $str;
}

function getRemoteFileSize($url){
    $arrRequest = parse_url($url);
    // protokol mora biti http ili https
    if($arrRequest['scheme'] != 'http' && $arrRequest['scheme'] != 'https'){
        die(parseError('Ne mogu pronaci datoteku na udaljenom serveru :S'));
    }
    // ako port nije postavljen ili je prazan postavi ga na 80
    if(!isset($arrRequest['port']) || empty($arrRequest['port'])){
        $arrRequest['port'] = 80;
    }

    $fp = fsockopen($arrRequest['host'], $arrRequest['port'], $errno, $errstr, 30);

    // ako veza nije uspostavljena vrati -1
    if (!$fp) {
        // ako hoce ispii i greku
        // echo "$errstr ($errno)<br />\n";
        return -1;
    }
    // kreiraj http zahtjev
    $httpRequest = 'HEAD '.$arrRequest['path']."  HTTP/1.1\r\n";
    $httpRequest .= 'Host: '.$arrRequest['host']."\r\n";
    $httpRequest .= "Connection: Close\r\n\r\n";

    // poalji http zahtjev
    fwrite($fp, $httpRequest);

    // uzmi sve dobivene
    $returnedData = '';
    while (!feof($fp)) {
        $returnedData .= fgets($fp, 128);
    }
    fclose($fp);

    $arrHeaders = parseHttpHeader($returnedData);
    // ako postoji postavka "content-length" vrati nju, ako ne vrati -1
    if(isset($arrHeaders['content-length'])){
        return (int) $arrHeaders['content-length'];
    }else{
        return -1;
    }
}

function parseError($str){
    return "<font size = '12' color = 'red'>{$str}</font>";
}

function parseHttpHeader($string){
    // kraj zaglavlja http zahtjeva definiran je s "\r\n\r\n" nizom znakova
    $endOfHeaders = strpos($string, "\r\n\r\n");
    if($endOfHeaders == -1){
        die('Error');
    }
    // izvuci samo zaglavlje iz podataka
    $string = substr($string, 0, $endOfHeaders);
    // postavke u http zaglavlju su razdvojene s "\r\n"
    $headerRows = explode("\r\n", $string);
    $headerRows[0] .= ': ';
    $arrHeaders = array();
    // prodi kroz svaki red i razdvoji kljuc od vrijednosti
    while(list(, $f_value) = each($headerRows)){
        $row = explode(":", $f_value, 2);
        $arrHeaders[strtolower(trim($row[0]))] = trim($row[1]);
    }
    return $arrHeaders;
}

function fb_desc_process($description, $limit = 250) {
    $search = array("\"", "\r\n", "\r", "\n");
    $replace = array("'", " ", " ", " ");

    return str_replace($search, $replace, html_entity_decode($description, ENT_QUOTES, 'UTF-8'));
    /*     return str_replace($search, $replace, html_entity_decode($description), ENT_QUOTES, 'UTF-8'); */
}