<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersOnline extends Model
{
    protected $table = 'korisnikaOnline';

    public $timestamps = false;
}
