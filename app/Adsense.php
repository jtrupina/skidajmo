<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adsense extends Model
{
    protected $table = 'adsense';

    public $timestamps = false;
}
