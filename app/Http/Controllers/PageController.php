<?php

namespace App\Http\Controllers;

use App\Category;
use App\Content;
use App\Software;
use App\UsersOnline;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Week;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class PageController extends Controller
{
    public function home($category = null)
    {
        $select = 'software.ime, dodano, verzija, velicina, opis as opis, nasaOcjena, software.imeUrl as simeUrl, kategorije.imeUrl as kimeUrl, tjedan, software_id, slika, updated';
        $select1 = 'software.id, ime, dodano, verzija, velicina, opis as opis, nasaOcjena, software.imeUrl as simeUrl, kategorije.imeUrl as kimeUrl, slika, updated';
        if(App::getLocale('applocale') == 'en') {
            $select = 'software.ime, dodano, verzija, velicina, opisEn as opis, nasaOcjena, software.imeUrl as simeUrl, kategorije.imeUrl as kimeUrl, tjedan, software_id, slika, updated';
            $select1 = 'software.id, ime, dodano, verzija, velicina, opisEn as opis, nasaOcjena, software.imeUrl as simeUrl, kategorije.nameUrl as kimeUrl, slika, updated';
        }
        $categories = Category::with('softwares')->get();
        $softwareWeeks = DB::table('weeks')->join('software', 'weeks.software_id', '=', 'software.id')
            ->join('kategorije', 'kategorije.id', '=', 'software.kategorija')
            ->orderBy('tjedan', 'DESC')
            ->select(DB::raw($select))
            ->take(3)
            ->get();
        if(isset($category)) {
            $category = DB::table('kategorije')->where('imeUrl', $category)
                ->orWhere('nameUrl', $category)
                ->get();
            $softwares = DB::table('software')->join('kategorije', 'software.kategorija', '=', 'kategorije.id')
                ->where('kategorija', '=', $category[0]->id)
                ->where('dodano', '<', time())
                ->orderBy('dodano', 'desc')
                ->select(DB::raw($select1))
                ->paginate(8);
        } else {
            $softwares = DB::table('software')->join('kategorije', 'software.kategorija', '=', 'kategorije.id')
                ->where('dodano', '<', time())
                ->orderBy('dodano', 'desc')
                ->select(DB::raw($select1))
                ->paginate(8);
        }
        //dd($softwares);
        return view('home', compact('categories', 'softwares', 'softwareWeeks'));
    }

    public function details($category, $software)
    {
        $sw = Software::where('imeUrl', $software)->first();
        $c = Category::where('imeUrl', $category)->orWhere('nameUrl', $category)->first();
        $newSoftwares = DB::table('software')->join('kategorije', 'software.kategorija', '=', 'kategorije.id')
            ->where('dodano', '<', time())
            ->where('kategorija', '=', $c->id)
            ->orderBy('dodano', 'desc')
            ->select(DB::raw('ime, software.imeUrl as simeUrl, kategorije.imeUrl as kimeUrl'))
            ->take(15)
            ->get();
        return view('details', compact('sw', 'c', 'newSoftwares'));
    }

    public function aboutUs()
    {
        $content = Content::find(1);
        $num1 = rand(1, 10);
        $num2 = rand(1, 10);
        return view('pages/aboutUs', compact('content', 'num1', 'num2'));
    }

    public function whatIsDownloaded()
    {
        $select1 = 'software.id, ime, dodano, verzija, velicina, nasaOcjena, software.imeUrl as simeUrl, kategorije.imeUrl as kimeUrl, slika, skidanja';
        if(App::getLocale('applocale') == 'en') {
            $select1 = 'software.id, ime, dodano, verzija, velicina, nasaOcjena, software.imeUrl as simeUrl, kategorije.nameUrl as kimeUrl, slika, skidanja';
        }
        $softwares = DB::table('software')->join('kategorije', 'software.kategorija', '=', 'kategorije.id')
            ->orderByRaw(DB::raw('RAND()'))
            ->select(DB::raw($select1))
            ->take(30)
            ->get();
        return view('pages/currentlyDownloaded', compact('softwares'));
    }

    public function addSoftware()
    {
        $num1 = rand(1, 10);
        $num2 = rand(1, 10);
        return view('pages/addSoftware', compact('num1', 'num2'));
    }

    public function topPrograms()
    {
        $select1 = 'software.id, ime, dodano, verzija, velicina, nasaOcjena, software.imeUrl as simeUrl, kategorije.imeUrl as kimeUrl, slika, skidanja';
        if(App::getLocale('applocale') == 'en') {
            $select1 = 'software.id, ime, dodano, verzija, velicina, nasaOcjena, software.imeUrl as simeUrl, kategorije.nameUrl as kimeUrl, slika, skidanja';
        }
        $softwares = DB::table('software')->join('kategorije', 'software.kategorija', '=', 'kategorije.id')
            ->orderBy('skidanja', 'desc')
            ->select(DB::raw($select1))
            ->take(50)
            ->get();
        return view('pages/topPrograms', compact('softwares'));
    }
}
