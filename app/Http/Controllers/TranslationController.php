<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class TranslationController extends Controller
{
    /**
     * Change session locale
     * @param  Request $request
     * @return Response
     */
    public function changeLocale(Request $request)
    {
        $this->validate($request, ['applocale' => 'required|in:en,hr']);

        Session::put('applocale', $request->applocale);

        return redirect()->back();
    }
}
