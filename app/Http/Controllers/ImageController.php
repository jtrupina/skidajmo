<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ImageController extends Controller
{
    public $path;
    public $folder;
    public $ext;
    public $w;
    public $h;
    public $ime;
    public $thumbPath;
    public $imagePathName;

    public function upload($pth, $polje = "file")
    {
        $this->folder = $pth;
        $arr = explode(".", $_FILES[$polje]['name']);
        $this->ext = $arr[count($arr) - 1];
        $this->path = $pth . "/originals/" . $_FILES[$polje]['name'];
        $this->imagePathName = "/" . $_FILES[$polje]['name'];
        $this->ime = str_replace($this->ext, "", $_FILES[$polje]['name']);
        if (file_exists($this->path))
        {
            while (file_exists($this->path))
            {
                $this->path = $pth . "/" . rand(100, 999) . "_" . rand(100, 999) . "." . $this->
                    ext;
                $this->imagePathName = "/" . rand(100, 999) . "_" . rand(100, 999) . "." . $this->
                    ext;
            }
        }
        if(file_exists($this -> path)){
            print "Ova slika vec postoji na serveru, molimo vas, preimenujte ju!";
            exit;
        }
        move_uploaded_file($_FILES[$polje]['tmp_name'], $this->path);
        $size = getimagesize($this->path);
        $this->w = $size[0];
        $this->h = $size[1];
    }

    // RENAME SLIKE
    public function preimenuj($novoime)
    {
        if (file_exists("$this->folder/$novoime.$this->ext"))
        {
            exit("Slika sa tim imenom vec postoji");
        }
        rename($this->path, "$this->folder/$novoime.$this->ext");
        $this->path = "$this->folder/$novoime.$this->ext";
    }

    // BRISANJE SLIKE
    public function obrisi($pth)
    {
        unlink($pth);
    }

    // RESIZE
    public function resize($nwidth, $nheight = 0, $proporcionalno = 1)
    {
        $size = getimagesize($this->path);
        $this->w = $size[0];
        $this->h = $size[1];
        if ($proporcionalno == 1)
        {
            $nheight = round($this->h / ($this->w / $nwidth));
        }
        switch ($this->ext)
        {
            case "JPEG":
            case "jpeg":
            case "jpg":
            case "JPG":
                $slika = imagecreatefromjpeg($this->path);
                $dimenzije = imagecreatetruecolor($nwidth, $nheight);
                imagecopyresampled($dimenzije, $slika, 0, 0, 0, 0, $nwidth, $nheight, $this->w,
                    $this->h);
                unlink($this->path);
                return imagejpeg($dimenzije, $this->path);
                break;
            case "GIF":
            case "gif":
                $slika = imagecreatefromgif($this->path);
                $dimenzije = imagecreatetruecolor($nwidth, $nheight);
                imagecopyresampled($dimenzije, $slika, 0, 0, 0, 0, $nwidth, $nheight, $this->w,
                    $this->h);
                unlink($this->path);
                return imagegif($dimenzije, $this->path);
                break;
            case "PNG":
            case "png":
                $slika = imagecreatefrompng($this->path);
                $dimenzije = imagecreatetruecolor($nwidth, $nheight);
                imagecopyresampled($dimenzije, $slika, 0, 0, 0, 0, $nwidth, $nheight, $this->w,
                    $this->h);
                unlink($this->path);
                return imagepng($dimenzije, $this->path);
                break;
        }
    }
    // THUMBNAIL
    public function createThumb($folder, $width, $height = 0, $proporcionalno = 1){
        copy($this->path, $folder . $this->imagePathName);
        $this->path = $folder . $this->imagePathName;
        $this->resize($width, $height, $proporcionalno);
        $this->tmbPath = $this->path;
    }
    public function getPath(){
        return $this -> path;
    }

    public function getImagePathName()
    {
        return $this->imagePathName;
    }
}
