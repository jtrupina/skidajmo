<?php

namespace App\Http\Controllers;

use App\Software;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{

    CONST NUM = 5;

    public static function getMenu()
    {
        $categories = Category::with('softwares')->orderBy('imeKategorije')->get();
        $softwares = DB::table('software')->join('kategorije', 'software.kategorija', '=', 'kategorije.id')
            ->where('preporuka', '=', 1)
            ->where('dodano', '<', time())
            ->orderBy(DB::raw('RAND()'), 'desc')
            ->select(DB::raw('software.id, ime, dodano, verzija, velicina, opis, nasaOcjena, software.imeUrl as simeUrl, kategorije.imeUrl as kimeUrl, slika'))
            ->take(5)
            ->get();
        $popularSoftwares = DB::table('software')->join('kategorije', 'software.kategorija', '=', 'kategorije.id')
            ->orderBy('skidanja', 'desc')
            ->select(DB::raw('ime, software.imeUrl as simeUrl, kategorije.imeUrl as kimeUrl'))
            ->take(6)
            ->get();

        return view('menu', compact('categories', 'softwares', 'popularSoftwares'));
    }

    public static function getStars($number)
    {
        $stars = "";
        for ($i = 0; $i < $number; $i++) {
            $stars .= "<span class='evaluation_point'> </span>";
        }
        $stars .= self::numberOfNonStars(self::NUM - $number);

        return $stars;
    }

    private static function numberOfNonStars($number)
    {
        $stars = '';
        for ($i = 0; $i < $number; $i++) {
            $stars .= "<span class='evaluation_not'></span>";
        }

        return $stars;
    }

    public static function getTotalNumberOfSoftware()
    {
        $softwareCount = Software::count();

        return $softwareCount;
    }

    public static function getTotalNumberOfFavorites($userId)
    {
        $favoritesCount = DB::select('select count(*) as total from favorites where korisnici_id= ?', [$userId]);
        $favoritesCount = $favoritesCount[0]->total;

        return $favoritesCount;
    }

    public static function getTotalNumberOfCategories()
    {
        $categorieCount = Category::count();

        return $categorieCount;
    }

    public static function getTotalSizeOfSoftware()
    {
        $totalSize = DB::select('select sum(velicina) as total from software');
        $totalSize = $totalSize[0]->total;
        $totalSize = number_format($totalSize / 1024 / 1024 / 1014, 2) . ' GB';

        return $totalSize;
    }

    public static function getNumberOfFbFans()
    {
        //$url = "https://graph.facebook.com/v2.1/skidajmo.com?fields=likes&access_token=EAACEdEose0cBAE9ZCO75MkW0dCkNruYWpSZAS5rFrgebz0WkFwvvYe4WfdiiWYZBxuNVfwQGjhX4fRClR2rkGui1fVntvG2thLZBx7aYVygkbJ7SwMmZCfIHtCH32hQkBEVzbq1ea648poZCvpCvqqBssBloOzZAZBBuv2tOfK58egZDZD";
        //$response = file_get_contents($url);
        //$json = json_decode($response, true);

        //return $json['likes'];

        return "";
    }

    public static function getTop13ProgramsTags()
    {
        $softwares = Software::orderBy('skidanja', 'DESC')->take(13)->get();
        $output = "";
        foreach($softwares as $key => $software) {
            $shrinkedName = substr($software->ime,0,5) . '...';
            $key = $key + 1;
            $output .= "<button class='btn{$key}'>{$shrinkedName}</button>";
        }
        $output .= "<a class='topPrograms'>TOP 50 programa</a>
                    <span class='image'></span>
                    </div>";
        return $output;
    }

    public static function getFiveMostRecentQuestions()
    {
        $db_ext = \DB::connection('mysql2');
        $questions = $db_ext->table('sforum_posts')->select('id', 'title')->where('approved', 'y')->where('is_question', 'y')->where('flagged', 'n')->orderBy('date', 'DESC')->take(5)->get();
        $html = '';
        foreach($questions as $question) {
            $html .= "<li class='question-item'><a href='{$_SERVER['HTTP_HOST']}/pitanja/thread.php?id={$question->id}'><span class='question-text'>{$question->title}</span></a></li>";
        }
        return $html;
    }
}
