<?php

namespace App\Http\Controllers\Api;

use App\Software;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;

class SearchSoftwareController extends Controller
{
    public function searchByName($name)
    {
        $softwares = Software::where('ime','like','%'.$name.'%')
            ->orderBy('ime','asc')
            ->get(array('ime','verzija','imeUrl'));
        return $softwares;
    }

    public function getSoftwaresByName($name)
    {
        $softwares = Software::where('ime','like','%'.$name.'%')
            ->orderBy('ime','asc')
            ->get(array('ime','verzija','imeUrl'));
        return response()->json($softwares);
    }

    public function setCookieExperience()
    {
        return response("OK")->withCookie(cookie()->forever('exper', 'experience'));
    }
}
