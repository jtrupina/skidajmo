<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ContactController extends Controller
{
    public function contact(Request $request)
    {
        if(empty($request->name) || empty($request->email) || empty($request->phone) || empty($request->comments) || empty($request->title)) {
            return redirect()->back()->with('fail', 'Sva polja su obavezna!');
        }
        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            return redirect()->back()->with('fail', 'Email je neispravnog formata');
        }
        $sum = $request->num1 + $request->num2;
        if($request->sum != $sum) {
            return redirect()->back()->with('fail', 'Provjera nije prošla. Molimo pokušajte ponovo');
        }

        $this->sendEmail($request);

        return redirect()->back()->with('success', true);
    }

    private function sendEmail($request) {

        $recipient = 'skidajmo.com@gmail.com';

        // now replace the double square-bracket-enclosed variables with the real thing.
        $body = "[[name]] ([[email]]) poslana poruka sa naslovom [[subject]]: \n\n\"[[content]]\"";
        $subject = "Mail od [[name]]: [[subject]]";
        $from = $request->email;

        $sender_body = '';
        $sender_subject = '';


        $body = str_replace("[[name]]", $request->name, $body);
        $body = str_replace("[[email]]", $request->email, $body);
        $body = str_replace("[[subject]]", $request->title, $body);
        $body = str_replace("[[content]]", $request->comments, $body);
        $subject = str_replace("[[name]]", $request->name, $subject);
        $subject = str_replace("[[subject]]", $request->title, $subject);


        $headers = "From: {$from}\r\nReply-To: {$from}\r\n";
        @mail( $recipient, $subject, $body, $headers );

        return true;

    }
}
