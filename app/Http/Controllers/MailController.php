<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendMail()
    {
        $title = 'test';
        $content = 'testna poruka';

        Mail::send('emails.send', ['title' => $title, 'content' => $content], function ($message)
        {

            $message->from('pero@hotmail.com', 'Pero Perić');

            $message->to('jtrupina@gmail.com')->subject('test');

        });

        return "OK";
    }
}
