<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Software;
use Illuminate\Support\Facades\DB;
use App\Category;

use App\Http\Requests;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $softwares = DB::table('software')
            ->select(array('ime', 'verzija', 'imeUrl'))
            ->where('ime','like','%'.$request->q.'%')
            ->paginate(20);
        return view('searchResult', compact('softwares'));
    }

    public function show($software)
    {
        $sw = Software::where('imeUrl', $software)->first();
        $category = Category::find($sw->kategorija);
        $c = Category::where('imeUrl', $category->imeUrl)->first();
        $newSoftwares = DB::table('software')->join('kategorije', 'software.kategorija', '=', 'kategorije.id')
            ->where('dodano', '<', time())
            ->where('kategorija', '=', $c->id)
            ->orderBy('dodano', 'desc')
            ->select(DB::raw('ime, software.imeUrl as simeUrl, kategorije.imeUrl as kimeUrl'))
            ->take(15)
            ->get();
        return view('details', compact('sw', 'c', 'newSoftwares'));
    }
}
