<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FavoritesController extends Controller
{
    public function index()
    {
        if(!Auth::user()) {
            return redirect('social/login/redirect/facebook');
        }

        $user = Auth::user();
        $favorites = $user->favorites;
        return view('favorites', compact('favorites'));
    }

    public function delete($id)
    {
        $user = Auth::user();
        $user->favorites()->detach($id);
        return redirect('str/software_list');
    }

    public function deleteAll()
    {
        $user = Auth::user();
        $user->favorites()->detach();
        return redirect('str/software_list');
    }

    public function addFavorite($id)
    {
        $user = Auth::user();
        $user->favorites()->attach($id);
        return redirect()->back();
    }

    public static function checkFavorite($id)
    {
        $addFavorite = "";
        $user = Auth::user();
        $res = DB::table('favorites')->where('korisnici_id', $user->id)->where('software_id', $id)->get();
        $url = route('add.favorite', [$id]);
        if(!empty($res)) {
            $addFavorite .= "";
        } else {
            $addFavorite .= "<a href='$url'><button class='more'>+</button></a>";
        }
        return $addFavorite;
    }

    public static function getCategoryUrl($categoryId)
    {
        $category = Category::find($categoryId);
        if(App::getLocale('applocale') == 'en') {
            return $category->nameUrl;
        }
        return $category->imeUrl;
    }
}
