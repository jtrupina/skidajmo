<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Cache;

use App\Http\Requests;
use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Feed;
use Illuminate\Support\Facades\DB;
use Suin\RSSWriter\Item;

class RssFeedController extends Controller
{
    public function getRSS()
    {
        if (Cache::has('rss-feed')) {
            //return Cache::get('rss-feed');
        }

        $rss = $this->buildRssData();
        Cache::add('rss-feed', $rss, 120);

        return response($rss)->header('Content-type', 'application/rss+xml');
    }

    protected function buildRssData()
    {
        $now = Carbon::now();
        $feed = new Feed();
        $channel = new Channel();
        $channel
            ->title('Skidajmo.com | RSS kanal')
            ->description('RSS kanal Skidajmo.com | download portal')
            ->url('http://www.skidajmo.com/')
            ->lastBuildDate($now->timestamp)
            ->appendTo($feed);

        foreach($this->getRssData() as $data) {
            $item = new Item();
            $item
                ->title($data->ime)
                ->url('http://' .$_SERVER['HTTP_HOST'] . '/' . $data->kimeUrl . '/' . $data->simeUrl)
                ->description($data->opis)
                ->appendTo($channel);
        }

        $feed = (string)$feed;

        // Replace a couple items to make the feed more compliant
        $feed = str_replace(
            '<rss version="2.0">',
            '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">',
            $feed
        );
        $feed = str_replace(
            '<channel>',
            '<channel>'."\n".'    <atom:link href="'.url('/rss').
            '" rel="self" type="application/rss+xml" />',
            $feed
        );

        return $feed;
    }

    protected function getRssData() {
        $softwares = DB::table('software')->join('kategorije', 'software.kategorija', '=', 'kategorije.id')
            ->orderBy('dodano', 'desc')
            ->select(DB::raw('software.id, ime, dodano, verzija, velicina, opis as opis, nasaOcjena, software.imeUrl as simeUrl, kategorije.imeUrl as kimeUrl, kategorije.nameUrl as kimeEnUrl, slika'))
            ->where('dodano', '<=', time())
            ->take(15)
            ->get();

        return $softwares;
    }

}
