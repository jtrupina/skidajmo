<?php

namespace App\Http\Controllers;

use App\Software;
use App\UserSoftware;
use Illuminate\Http\Request;

use App\Http\Requests;

class AddSoftwareController extends Controller
{
    public function addSoftware(Request $request)
    {
        if(empty($request->name) || empty($request->email) || empty($request->description) || empty($request->address_software)) {
            return redirect()->back()->with('fail', 'Sva polja su obavezna!');
        }
        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            return redirect()->back()->with('fail', 'Email je neispravnog formata');
        }
        $sum = $request->num1 + $request->num2;
        if($request->sum != $sum) {
            return redirect()->back()->with('fail', 'Provjera nije prošla. Molimo pokušajte ponovo');
        }

        $userSoftware = new UserSoftware();
        $userSoftware->name = $request->name;
        $userSoftware->email = $request->email;
        $userSoftware->description = $request->description;
        $userSoftware->address_software = $request->address_software;
        $userSoftware->date = date('Y-m-d H:i:s');
        $userSoftware->save();

        return redirect()->back()->with('success', true);
    }

    public function showUserSoftwares()
    {
        $userSoftwares = UserSoftware::paginate(20);
        return view('admin.userSoftwares.index', compact('userSoftwares'));
    }

    public function deleteUserSoftware($id)
    {
        UserSoftware::findOrFail($id)->delete();
        return redirect('admin/korisnici-software');
    }

    public function acceptUserSoftware($id)
    {
        $userSoftware = UserSoftware::find($id);
        $userSoftware->approved = 1;
        $userSoftware->save();
        return redirect('admin/korisnici-software');
    }
}
