<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Software;
use Illuminate\Support\Facades\DB;

class DownloadController extends Controller
{
    public function download($id)
    {
        $softwares = Software::orderByRaw(DB::raw('RAND()'))->take(30)->get();
        $software = Software::find($id);
        return view('pages/download', compact('softwares', 'software'));
    }

    public function downloadMirror($id)
    {
        $softwares = Software::orderByRaw(DB::raw('RAND()'))->take(30)->get();
        $software = Software::find($id);
        return view('pages/downloadMirror', compact('softwares', 'software'));
    }

    public function downloadSoftware($id)
    {
        $software = Software::find($id);

        return redirect()->to($software->path);
    }

    public function downloadSoftwareFromServer($id)
    {
        $software = Software::find($id);

        //PDF file is stored under project/public/download/info.pdf
        $file= asset('download/' . $software->server_path);

        $headers = array(
            "Content-Type: " . $software->mime,
        );

        return response()->download($file, $software->server_path, $headers);
    }
}
