<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\ImageController;
use App\Software;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SoftwareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $softwares = Software::orderBy('dodano', 'desc')->paginate(10);
        $searching = false;
        return view('admin.software.index', compact('softwares', 'searching'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all(['id', 'imeKategorije']);
        return view('admin.software.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $software = new Software();
        $software->ime = $request->ime;
        $software->imeUrl = parseCategoryName($request->ime);
        $software->kategorija = $request->kategorija;
        $software->autor = $request->autor;
        $software->homepage = $request->homepage;
        $software->verzija = $request->verzija;
        $software->licenca = $request->licenca;
        $software->platforma = $request->platforma;
        if(!empty($request['path'])) {
            $software->velicina = getRemoteFileSize($request['path']);
        }
        $software->sucelje = $request->sucelje;
        $software->mogucnosti = $request->mogucnosti;
        $software->jednostavnost = $request->jednostavnost;
        $software->nasaOcjena = $request->nasaOcjena;
        if(empty($request->preporuka)) {
            $software->preporuka = 0;
        } else {
            $software->preporuka = $request->preporuka;
        }
        $software->path = $request->path;
        $software->opis = $request->opis;
        $software->opisEn = $request->opisEn;
        $software->skidanja = $request->skidanja;
        $software->velicina = $request->velicina;
        if($request->updated == false) {
            $vrijeme = time();
            $software->stvoreno = $vrijeme;
            $software->dodano = $vrijeme;
        } else {
            $software->stvoreno = strtotime($request->updated);
            $software->dodano = strtotime($request->updated);
        }
        $file = $request->file('dl_file');
        if(isset($file)) {
            $software->server_path = $file->getClientOriginalName();
            $software->mime = $file->getClientMimeType();
            $file->move('download', $file->getClientOriginalName());
        }

        if($_FILES['file']['name'] == '') {
            return back()->with('message', 'Slika je obavezna');
        }
        $rand = time();
        $rand .= rand(0,9);
        $rand .= rand(0,9);
        $rand .= rand(0,9);
        $software->rand = $rand;
        $folder = public_path() . "/slike";
        $arr = explode(".", $_FILES["file"]['name']);
        $ext = $arr[count($arr) - 1];
        $ekstenzije = array("jpg","JPG","gif","GIF","jpeg","JPEG","png","PNG");
        if(in_array($ext, $ekstenzije)){
            $slika = new ImageController();
            $slika ->upload($folder);
            $slika->createThumb($folder, 200, 122);
            $path = substr($slika -> getPath(), 9,strlen($slika -> getPath()));
            $software->slika = $slika->getImagePathName();
        }
        else{
            return back()->with('message', "Ekstenzija {$ext} nije dozvoljena!");
        }
        $software->save();
        return redirect('admin/software');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $software = Software::find($id);
        $categories = Category::all();
        return view('admin.software.edit', compact('software', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $software = Software::find($id);
        $software->ime = $request->ime;
        $software->imeUrl = parseCategoryName($request->ime);
        $software->kategorija = $request->kategorija;
        $software->autor = $request->autor;
        $software->homepage = $request->homepage;
        $software->verzija = $request->verzija;
        $software->licenca = $request->licenca;
        $software->platforma = $request->platforma;
        $software->sucelje = $request->sucelje;
        $software->mogucnosti = $request->mogucnosti;
        $software->jednostavnost = $request->jednostavnost;
        $software->nasaOcjena = $request->nasaOcjena;
        $software->preporuka = $request->preporuka;
        $software->path = $request->path;
        $software->opis = $request->opis;
        $software->opisEn = $request->opisEn;
        $software->skidanja = $request->skidanja;
        $software->velicina = $request->velicina;
        if($request->updated == false) {
            $vrijeme = time();
            $software->stvoreno = $vrijeme;
            $software->dodano = $vrijeme;
        } else {
            $software->stvoreno = strtotime($request->updated);
            $software->dodano = strtotime($request->updated);
        }
        $file = $request->file('dl_file');
        if(isset($file)) {
            $software->server_path = $file->getClientOriginalName();
            $software->mime = $file->getClientMimeType();
            $file->move('download', $file->getClientOriginalName());
        }
        $folder = public_path() . "/slike";
        $arr = explode(".", $_FILES["file"]['name']);
        $ext = $arr[count($arr) - 1];
        $ekstenzije = array("jpg","JPG","gif","GIF","jpeg","JPEG","png","PNG");
        if(in_array($ext, $ekstenzije)){
            $slika = new ImageController();
            $slika -> upload($folder);
            $slika->createThumb($folder, 200, 122);
            $path = substr($slika -> getPath(), 9,strlen($slika -> getPath()));
            $software->slika = $slika->getImagePathName();
        }
        $software->updated = date('Y-m-d H:i:s', time());
        $software->save();
        return redirect('admin/software');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Software::findOrFail($id)->delete();
        return redirect('admin/software');
    }

    /**
     * Search software by given name
     *
     * @param Request $request
     */
    public function search(Request $request)
    {
        $searching = true;
        $searchedTerm = '%' . $request['ime'] . '%';
        $softwares = Software::where('ime', 'LIKE', $searchedTerm)->get();
        return view('admin.software.index', compact('softwares', 'searching'));
    }
}
