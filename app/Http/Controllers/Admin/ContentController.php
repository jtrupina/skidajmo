<?php

namespace App\Http\Controllers\Admin;

use App\Content;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    public function index()
    {
        $content = Content::find(1);
        return view('admin.content.content', compact('content'));
    }

    public function saveContent(Request $request)
    {
        $content = Content::find(1);
        $content->content = $request->contents;
        $content->save();

        return redirect()->back();
    }
}
