<?php

namespace App\Http\Controllers\Admin;

use App\Software;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DeadLinksController extends Controller
{
    public function index()
    {
        $softwares = Software::where('broken', 1)->get(['id','ime']);
        return view('admin.deadlinks.index', compact('softwares'));
    }

    public function checkLinks()
    {
        $time_start = microtime(true);
        $softwares = Software::all(['id', 'ime', 'path']);
        foreach($softwares as $software) {
            if(!$this->checkIfLinkIsDead($software->path)) {
                $software->broken = 1;
                $software->save();
            }
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return "Završena provjera mrtvih linkova: $time";
    }

    public function updateLink(Request $request, $id)
    {
        if(empty($request->path) || $request->path == '') {
            redirect()->back()->with('message', 'Niste unijeli path!');
        }
        $software = Software::find($id);
        $software->path = $request->path;
        $software->broken = 0;
        $software->save();

        return redirect()->back();
    }

    public function reportBrokenLink($id)
    {
        $software = Software::find($id);
        $software->broken = 1;
        $software->save();

        return redirect()->back();
    }

    protected function checkIfLinkIsDead($path)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        $data = curl_exec($ch);
        curl_close($ch);
        preg_match_all("/HTTP\/1\.[1|0]\s(\d{3})/",$data,$matches);
        $code = end($matches[1]);

        if(!$data)
        {
            return(false);
        }
        else
        {
            if($code==200)
            {
                return(true);
            }
            elseif($code==404)
            {
                return(false);
            }
        }
    }

    public function deleteSoftware($id)
    {
        Software::findOrFail($id)->delete();
        return redirect()->back();
    }
}
