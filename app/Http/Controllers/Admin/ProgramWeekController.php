<?php

namespace App\Http\Controllers\Admin;

use App\Week;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProgramWeekController extends Controller
{
    public function index()
    {
        $softwareWeeks = Week::with('software')->orderBy('tjedan', 'DESC')->get();

        return view('admin.weekprograms.index', compact('softwareWeeks'));
    }

    public function addProgram(Request $request, $id)
    {
        $softwareWeek = new Week();
        $softwareWeek->tjedan = time();
        $softwareWeek->software_id = $id;
        $softwareWeek->ime = $request->ime;
        $softwareWeek->save();

        return redirect()->action('Admin\ProgramWeekController@index');
    }
}
