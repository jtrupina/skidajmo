<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class MarksController extends Controller
{
    const NUM = 5;

    public function addMark(Request $request)
    {
        $mark = $request->input('mark');
        $softwareId = $request->input('softwareId');
        $userId = $request->input('userId');

        $cnt = DB::table('ocjene')->where('korisnici_id', $userId)->where('software_id', $softwareId)->count();

        $user = User::find($userId);
        if($cnt) {
            $user->marks()->updateExistingPivot($softwareId, ['ocjena' => $mark]);
        } else {
            $user->marks()->attach($softwareId, ['ocjena' => $mark]);
        }

        $countVotes = DB::table('ocjene')->where('software_id', $softwareId)->count();
        $sumMarks = DB::table('ocjene')->where('software_id', $softwareId)->average('ocjena');
        $sumMarks = round($sumMarks, 2);

        return json_encode(array('votes' => $countVotes, 'marks' => $sumMarks));
    }

    public static function getMarks($id)
    {
        $user = Auth::user();
        $marks  = "";
        $langVotes = "";
        if(App::getLocale('applocale') == 'hr') {
            $langVotes = 'glasova';
        } else {
            $langVotes = 'votes';
        }
        if($user) {
            $res = DB::table('ocjene')->where('korisnici_id', $user->id)->where('software_id', $id)->first();
            $marks .= "<span class='evaluation'>";
            if(!empty($res)) {
                $marks .= self::getStars($id, $user->id, $res->ocjena);
            } else {
                $marks .= self::getStarsForVote($id, $user->id);
            }
            $marks .= "</span>";
        } else {
            $url = route('social.login', ['facebook']);
            if(App::getLocale('applocale') == 'hr') {
                $marks .= "<a href='$url' <span>Prijavite se u sustav!</span></a>";
            } else {
                $marks .= "<a href='$url'><span>You have to log in for rating!</span></a>";
            }
        }
        $countVotes = DB::table('ocjene')->where('software_id', $id)->count();
        $sumMarks = DB::table('ocjene')->where('software_id', $id)->average('ocjena');
        $sumMarks = round($sumMarks, 2);
        $marks .= "<div class='total-box'>
                    <span id='tp' class='total_points'>$sumMarks</span><br>
                    <span id='n' class='number_evaluation'>$countVotes $langVotes</span>
                </div>";
        $marks .= "</div></div>";
        return $marks;
    }

    public static function getStars($softwareId, $userId, $number)
    {
        $stars = "";
        for($i = 1; $i <= 5; $i++) {
            if($i <= $number) {
                $stars .= "<span class='evaluation_point' data-mark='$i' data-softwareId='$softwareId' data-userId='$userId'> </span>";
            } else {
                $stars .= "<span class='evaluation_not' data-mark='$i' data-softwareId='$softwareId' data-userId='$userId'></span>";
            }

        }
        //$stars .= self::numberOfNonStars($softwareId, $userId, self::NUM - $number);
        return $stars;
    }

    private static function numberOfNonStars($softwareId, $userId, $number) {
        $stars = '';
        for($i = 0; $i < $number; $i++) {
            $stars .= "<span class='evaluation_not' data-mark='$i' data-softwareId='$softwareId' data-userId='$userId'></span>";
        }
        return $stars;
    }

    private static function getStarsForVote($softwareId, $userId)
    {
        $stars = '';
        for($i = 1; $i <= 5; $i++) {
            $stars .= "<span class='evaluation_not' data-mark='$i' data-softwareId='$softwareId' data-userId='$userId'></span>";
        }
        return $stars;
    }
}
