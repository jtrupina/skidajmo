<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::get('mail', 'MailController@sendMail');
Route::get('test', 'TestController@index');
/* PAGES */
Route::post('changelocale', ['as' => 'changelocale', 'uses' => 'TranslationController@changeLocale']);
Route::post('dodaj', ['as' => 'dodaj.software', 'uses' => 'AddSoftwareController@addSoftware']);
Route::post('contact', 'ContactController@contact');
Route::get('RSS', 'RssFeedController@getRSS');
Route::get('/o-nama', 'PageController@aboutUs');
Route::get('str/sto-se-skida', 'PageController@whatIsDownloaded');
Route::get('/dodaj', 'PageController@addSoftware');
Route::get('/logout', 'Auth\AuthController@logout');
Route::get('/top-50', 'PageController@topPrograms');

Route::get('/downloads/{id}', ['uses' => 'DownloadController@download', 'as' => 'download.software']);
Route::get('/downloads/m/{id}', ['uses' => 'DownloadController@download', 'as' => 'download.software.mirror']);
Route::get('/downloads/download/{id}', ['uses' => 'DownloadController@downloadSoftware', 'as' => 'download.software.path']);
Route::get('/downloads/download/server/{id}', ['uses' => 'DownloadController@downloadSoftwareFromServer', 'as' => 'download.software.path.server']);

Route::get('/search', 'SearchController@index');
Route::get('/search/{software}', 'SearchController@show');
/* API */
Route::group(['prefix' => 'api/v1'], function() {
    Route::get('/search/{name}', 'Api\SearchSoftwareController@searchByName');
    Route::get('/getSoftwares/{name}', 'Api\SearchSoftwareController@getSoftwaresByName');
    Route::post('/cookie', 'Api\SearchSoftwareController@setCookieExperience');
});

Route::group(['middleware' => 'web'], function () {

    Route::auth();

    Route::get('home', ['as' => 'home', 'uses' => function () {
        return view('home');
    }]);

    Route::get('social/login/redirect/{provider}', ['uses' => 'Auth\AuthController@redirectToProvider', 'as' => 'social.login']);
    Route::get('social/login/{provider}', 'Auth\AuthController@handleProviderCallback');

    Route::get('str/software_list', 'FavoritesController@index');
    Route::get('str/software_list/delete/all', ['uses' => 'FavoritesController@deleteAll', 'as' => 'delete.favorite.all']);
    Route::get('str/software_list/delete/{id}', ['uses' => 'FavoritesController@delete', 'as' => 'delete.favorite']);
    Route::get('str/software_list/add/{id}', ['uses' => 'FavoritesController@addFavorite', 'as' => 'add.favorite']);

    Route::get('software/mark/add', ['uses' => 'MarksController@addMark', 'as' => 'add.mark']);



    # ADMIN SECTION
    Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {

        Route::get('home', 'HomeController@index');

        # CATEGORIES
        Route::resource('kategorije', 'Admin\CategoryController');

        # USERS
        Route::resource('korisnici', 'Admin\UserController');

        # SOFTWARE
        Route::resource('software', 'Admin\SoftwareController');
        Route::post('software/search', 'Admin\SoftwareController@search');

        #DEAD LINKS
        Route::get('deadlinks', 'Admin\DeadLinksController@index');
        Route::get('deadlinks/check', 'Admin\DeadLinksController@checkLinks');
        Route::post('deadlinks/update/{id}', ['uses' => 'Admin\DeadLinksController@updateLink', 'as' => 'deadlinks.update']);
        Route::delete('deadlinks/delete/{id}', ['uses' => 'Admin\DeadLinksController@deleteSoftware', 'as' => 'deadlinks.delete']);
        Route::post('deadlinks/report/{id}', ['uses' => 'Admin\DeadLinksController@reportBrokenLink', 'as' => 'deadlinks.report']);

        #PROGRAM OF WEEK
        Route::get('week/programs', 'Admin\ProgramWeekController@index');
        Route::post('week/programs/add/{id}', ['uses' => 'Admin\ProgramWeekController@addProgram', 'as' => 'week.program.add']);

        #USER SOFTWARE
        Route::get('korisnici-software', 'AddSoftwareController@showUserSoftwares');
        Route::delete('korisnici-software/delete/{id}', ['uses' => 'AddSoftwareController@deleteUserSoftware', 'as' => 'user.software.delete']);
        Route::get('korisnici-software/accept/{id}', ['uses' => 'AddSoftwareController@acceptUserSoftware', 'as' => 'user.software.accept']);

        #CONTENT
        Route::get('content', 'Admin\ContentController@index');
        Route::post('content', 'Admin\ContentController@saveContent');

    });

    Route::get('/{category?}', 'PageController@home');
    Route::get('/{category}/{software}', 'PageController@details');

});








