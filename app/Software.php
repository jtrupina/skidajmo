<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Software extends Model
{
    protected $table = 'software';

    protected $guarded = [];

    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('App\Category', 'kategorija');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'users', 'korisnici_id', 'software_id');
    }
}
