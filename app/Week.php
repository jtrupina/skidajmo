<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'weeks';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function software() {
        return $this->belongsTo('App\Software', 'software_id');
    }
}
