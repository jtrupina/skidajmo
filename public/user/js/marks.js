$(document).ready(function() {
    $('[class^="evaluation_"]').click(function(){
        var mark = $(this).data("mark");
        var softwareId = $(this).data("softwareid");
        var userId = $(this).data("userid");

        $.ajax({
            type: "GET",
            url: "http://localhost:8000/software/mark/add",
            data: {
                'mark' : mark,
                'softwareId' : softwareId,
                'userId' : userId
            },
            success: function(data) {
                var json = JSON.parse(data);
                document.getElementById("tp").innerHTML = json.marks;
                document.getElementById("n").innerHTML = json.votes + " glasova";
                $( ".evaluation-box1 .evaluation_point").each(function() {
                    $(this).attr('class', 'evaluation_not');
                });
                $( ".evaluation-box1 .evaluation_not").slice(0, mark).each(function() {
                    $(this).attr('class', 'evaluation_point');
                });
            }
        });

    });
});
