
*** September 15, 2012

1. Added the themes from http://bootswatch.com/
2. Added the option of having a classic forum home page
3. Added the option of having human friendly URLs for forums and threads
4. Added tinyMCE
5. Modified session variable name to allow for multiple instances of the forum on a server
6. Ehanced protection of forms from XSS attacks

*** October 10, 2012

1. Added a profile link to the sidebar to avoid confusion
2. Added the option for admin to have to approval all posts before they are displayed.
3. Made it possible for setting up of a multi-lingual forum
4. Fixed bug with the remember me feature of signing in
5. Allowed users to unlike a post after they have liked it

*** November 5, 2012 - Version 2.0

1. Added achievements
2. Added the moderator role
3. Allowed for threads to be pinned to the top of the list
4. Allowed the admin to manually order the forums
5. Allowed replying with quotes
6. Added a personal message system

*** January 21, 2013 - Version 2.1

1. Added facebook connect
2. Minor bug fix on threads page
3. Added captcha to the post form

Files that changed:

-php/config.php
-php/facebook/
-php/users_api.php
-php/recaptchalib.php
-php/functions.php
-includes.php
-posts.php
-thread.php
-facebook_login.php
-profile.php
-db_install.php
-html/posts.html
-html/recaptcha.html
-html/sidebar-signout.html
-html/base.html
-html/facebook_script.html
-html/profile.html
-str/strings.xml
-img/glyphicons/glyphicons_320_facebook.png

