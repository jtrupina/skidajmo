<?php

require 'includes.php';

if (!Users_IsUserLoggedIn()) {
    Leave(Users_SignInPageUrl());
}


$layout = GetPage('feed', '{{ST:my_feed}}');

if (defined('SITE_NAME') AND SITE_NAME != '') {
    $layout->AddContentById('meta_title', SITE_NAME);
}
if (defined('SITE_META_DESCRIPTION') AND SITE_META_DESCRIPTION != '') {
    $layout->AddContentById('meta_desc', SITE_META_DESCRIPTION);
}

$layout->AddContentById('breadcrumbs', ' <li><a href="' . FORUM_URL . '">{{ST:home}}</a></li><li class="active">{{ST:my_feed}}</li>');


if (isset($_GET['like']) AND intval($_GET['like']) == 1 AND isset($_GET['post'])) {
    if (Users_CurrentUserId()) {
        if (count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "likes WHERE user_id = " . intval(Users_CurrentUserId()) . " AND post_id = " . intval($_GET['post']) . "")) > 0) {

        } else {
            $db->insert(TABLES_PREFIX . "likes", array('post_id' => intval($_GET['post']), 'user_id' => intval(Users_CurrentUserId())), array("%d", "%d"));

            $the_post = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = " . intval($_GET['post']) . " ORDER BY id DESC LIMIT 0,1");
            $db->update(TABLES_PREFIX . "posts", array('likes' => (1 + $the_post->likes)), array('id' => intval($_GET['post'])), array("%d"));

            $layout->AddContentById('alert', $layout->GetContent('alert'));
            $layout->AddContentById('alert_nature', ' alert-success');
            $layout->AddContentById('alert_heading', '{{ST:success}}!');
            $layout->AddContentById('alert_message', '{{ST:you_have_liked_the_post}}');
        }
    }
}

if (isset($_GET['unlike']) AND intval($_GET['unlike']) == 1 AND isset($_GET['post'])) {
    if (Users_CurrentUserId()) {
        if (count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "likes WHERE user_id = " . intval(Users_CurrentUserId()) . " AND post_id = " . intval($_GET['post']) . "")) > 0) {
            $db->query("DELETE FROM " . TABLES_PREFIX . "likes WHERE post_id = " . intval($_GET['post']) . " AND user_id = " . intval(Users_CurrentUserId()));

            $the_post = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = " . intval($_GET['post']) . " ORDER BY id DESC LIMIT 0,1");
            $db->update(TABLES_PREFIX . "posts", array('likes' => ($the_post->likes - 1)), array('id' => intval($_GET['post'])), array("%d"));

            $layout->AddContentById('alert', $layout->GetContent('alert'));
            $layout->AddContentById('alert_nature', ' alert-success');
            $layout->AddContentById('alert_heading', '{{ST:success}}!');
            $layout->AddContentById('alert_message', '{{ST:you_have_unliked_the_post}}');
        }
    }
}

if (isset($_GET['delete']) AND intval($_GET['delete']) == 1 AND isset($_GET['post'])) {
    $the_post = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = " . intval($_GET['post']) . " ORDER BY id DESC LIMIT 0,1");

    if (Users_IsUserAdminOrModerator(Users_CurrentUserId()) OR Users_CurrentUserId() == $the_post->user_id) {

        if ($the_post->photos != '') {
            $files = unserialize($the_post->photos);
            if (count($files) > 0 AND is_array($files)) {
                foreach ($files as $f) {
                    $file = 'uploads/' . $f;
                    $exists = is_file($file);
                    if ($exists) {
                        unlink($file);
                    }
                }
            }
        }

        if ($the_post->is_question == 'y') {
            $all_children = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = " . intval($_GET['post']) . " OR parent_id = " . intval($_GET['post']) . "");
            if ($all_children) {
                foreach ($all_children as $child) {
                    $db->query("DELETE FROM " . TABLES_PREFIX . "posts_following WHERE post_id = " . intval($child->id));
                    $db->query("DELETE FROM " . TABLES_PREFIX . "likes WHERE post_id = " . intval($child->id));
                    $db->query("DELETE FROM " . TABLES_PREFIX . "flags WHERE post_id = " . intval($child->id));
                    $db->query("DELETE FROM " . TABLES_PREFIX . "posts WHERE id = " . intval($child->id));
                }
            }
            $layout->AddContentById('alert', $layout->GetContent('alert'));
            $layout->AddContentById('alert_nature', ' alert-success');
            $layout->AddContentById('alert_heading', '{{ST:success}}!');
            $layout->AddContentById('alert_message', '{{ST:the_post_has_been_deleted}}');
        } else {
            $db->query("DELETE FROM " . TABLES_PREFIX . "posts_following WHERE post_id = " . intval($_GET['post']));
            $db->query("DELETE FROM " . TABLES_PREFIX . "likes WHERE post_id = " . intval($_GET['post']));
            $db->query("DELETE FROM " . TABLES_PREFIX . "flags WHERE post_id = " . intval($_GET['post']));
            $db->query("DELETE FROM " . TABLES_PREFIX . "posts WHERE id = " . intval($_GET['post']));

            $layout->AddContentById('alert', $layout->GetContent('alert'));
            $layout->AddContentById('alert_nature', ' alert-success');
            $layout->AddContentById('alert_heading', '{{ST:success}}!');
            $layout->AddContentById('alert_message', '{{ST:the_post_has_been_deleted}}');
        }
    }
}

if (isset($_GET['flag']) AND intval($_GET['flag']) == 1 AND isset($_GET['post'])) {
    $flag_response = flag_a_post(intval($_GET['post']));

    if($flag_response['type'] == 'success'){
        $layout->AddContentById('alert', $layout->GetContent('alert'));
        $layout->AddContentById('alert_nature', ' alert-success');
        $layout->AddContentById('alert_heading', '{{ST:success}}!');
        $layout->AddContentById('alert_message', $flag_response['msg']);
    }else{
        $layout->AddContentById('alert', $layout->GetContent('alert'));
        $layout->AddContentById('alert_nature', ' alert-danger');
        $layout->AddContentById('alert_heading', '{{ST:error}}!');
        $layout->AddContentById('alert_message', $flag_response['msg']);
    }
}

$search = "";
$following = array();
$subscriptions = array();

$following_query = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "following WHERE follower_id =".Users_CurrentUserId()."" );
if($following_query){
    foreach($following_query as $q){
        $following[] = $q->user_id;
    }
}

$subscriptions_query = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts_following WHERE user_id =".Users_CurrentUserId()."" );
if($subscriptions_query){
    foreach($subscriptions_query as $q){
        $subscriptions[] = $q->post_id;
    }
}

if(count($following) > 0 AND count($subscriptions) > 0){
    $search = " AND (user_id IN (".implode(',', $following).") OR parent_id IN (".implode(',', $subscriptions)."))";
}else{
    if(count($following) > 0){
        $search = "AND user_id IN (".implode(',', $following).")";
    }elseif(count($subscriptions) > 0){
        $search = "AND parent_id IN (".implode(',', $subscriptions).")";
    }
}

$rows = ROWS_PER_PAGE;

if(count($following) > 0 OR count($subscriptions) > 0){
    $number_of_records = count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts WHERE approved = 'y' AND flags < ".HIDE_POST_ON_THIS_NUMBER_OF_FLAGS." $search"));
}else{
    $number_of_records = 0;
}
$number_of_pages = ceil($number_of_records / $rows);
if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

$offset = ($page - 1) * $rows;
$layout->AddContentById('page', $page);

if(count($following) > 0 OR count($subscriptions) > 0){
    $latest = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts WHERE approved = 'y' AND flags < ".HIDE_POST_ON_THIS_NUMBER_OF_FLAGS." $search ORDER BY date DESC LIMIT $offset, $rows");
}else{
    $latest = array();
}
$first = true;
$rows_html = '';
if ($latest) {
    foreach ($latest as $post) {
        $row_layout = new Layout('html/', 'str/');
        $row_layout->SetContentView('feed-rows');
        $row_layout->AddContentById('id', $post->id);

        if($first){
            $first = false;
            $layout->AddContentById('last_id', $post->id);
        }

        $row_layout->AddContentById('page', $page);

        if ($post->is_question == 'y') {
            $question = $post;
        } else {
            $question = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = " . $post->parent_id . " ORDER BY id DESC LIMIT 0,1");
        }

        $row_layout->AddContentById('thread_id', $question->id);

        if ($post->photos != '') {
            $files = unserialize($post->photos);
            if (count($files) > 0 AND is_array($files)) {
                $files_lists = '<br/><p>';
                $files_count = 1;
                foreach ($files as $f) {
                    $files_lists .= '<img src="' . get_file_icon_path($f) . '">&nbsp;<a target="_blank" href="' . FORUM_URL . 'uploads/' . $f . '">{{ST:attachment}} ' . $files_count . '</a>&nbsp;<br/>';
                    $files_count++;
                }
                $files_lists .= '</p>';
                $row_layout->AddContentById('files', $files_lists);
            }
        }


        $signature = "";

        $row_layout->AddContentById('likes', NiceNumber($post->likes));

        $row_layout->AddContentById('date', getRelativeTime($post->date));

        $row_layout->AddContentById('user_id', $post->user_id);

        $user_details = Users_GetUserDetails($post->user_id);
        if ($user_details) {
            if($user_details['is_admin'] == true){
                $row_layout->AddContentById('is_admin', '<img title="{{ST:is_admin}}" data-toggle="tooltip" src="{{ID:base_url}}img/admin.png">');
            }elseif($user_details['is_moderator'] == true){
                $row_layout->AddContentById('is_admin', '<img title="{{ST:moderator}}" data-toggle="tooltip" src="{{ID:base_url}}img/moderator.png">');
            }
            if ($user_details['username']) {
                $row_layout->AddContentById('user_name', $user_details['username']);
            }
            if ($user_details['path_to_profile']) {
                $row_layout->AddContentById('path_to_profile', $user_details['path_to_profile']);
            }
            if ($user_details['path_to_photo']) {
                $row_layout->AddContentById('user_photo', $user_details['path_to_photo']);
            } else {
                $row_layout->AddContentById('user_photo', FORUM_URL . 'img/anon.png');
            }

            if ($user_details['signature']) {
                $signature = "<hr/><div class='muted'>" . $user_details['signature'] . "</div>";

                $row_layout->AddContentById('signature', $signature);
            }
        }

        if (defined('SEO_HUMAN_FRIENDLY_URLS') AND SEO_HUMAN_FRIENDLY_URLS == true) {
            $thread_url = '{{ID:base_url}}thread/' . UrlText($post->title) . '/' . $post->id . '/';
        } else {
            $thread_url = '{{ID:base_url}}thread.php?id=' . $post->id;
        }

        if ($post->quote AND $post->quote != '') {
            $row_layout->AddContentById('question', '<h4>{{ST:thread_title}}: <a href="' . $thread_url . '">' . $question->title . '</a></h4>' . $post->quote . $post->body);
        } else {
            $row_layout->AddContentById('question', '<h4>{{ST:thread_title}}: <a href="' . $thread_url . '">' . $question->title . '</a></h4>' . $post->body);
        }

        $badges =  Users_GetUserBadges($post->user_id);
        if(!$badges){
            $row_layout->AddContentById('user_badges', '');
            $row_layout->AddContentById('display_badges', 'style="display: none;"');
        }else{
            $row_layout->AddContentById('user_badges', $badges);
        }

        if (!Users_IsUserAdminOrModerator(Users_CurrentUserId())) {
            if (Users_CurrentUserId() != intval($post->user_id)) {
                $row_layout->AddContentById('edit_state', 'style="display:none"');
                $row_layout->AddContentById('delete_state', 'style="display:none"');
            }
        }

        if (!Users_IsUserLoggedIn()) {
            $row_layout->AddContentById('like_or_un_get', 'like');
            $row_layout->AddContentById('like_or_un', 'btn-default');
            $row_layout->AddContentById('like_or_un_text', '{{ST:like}}');
            $row_layout->AddContentById('like_alert', 'onclick="return SignInAlert();"');
            $row_layout->AddContentById('flag_alert', 'onclick="return SignInAlert();"');
        } elseif (count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "likes WHERE user_id = " . intval(Users_CurrentUserId()) . " AND post_id = " . intval($post->id) . "")) > 0) {

            $row_layout->AddContentById('like_or_un_get', 'unlike');
            $row_layout->AddContentById('like_or_un', 'btn-success');
            $row_layout->AddContentById('like_or_un_text', '{{ST:unlike}}');
            $row_layout->AddContentById('flag_alert', 'onclick="return confirm(\'{{ST:are_you_sure}}\');"');
        } else {
            $row_layout->AddContentById('like_or_un_get', 'like');
            $row_layout->AddContentById('like_or_un', 'btn-default');
            $row_layout->AddContentById('like_or_un_text', '{{ST:like}}');
            $row_layout->AddContentById('flag_alert', 'onclick="return confirm(\'{{ST:are_you_sure}}\');"');
        }

        if ($question->locked == 'y') {
            $row_layout->AddContentById('post_alert', 'onclick="return LockedAlert();"');
        }


        $rows_html .= $row_layout->ReturnView();
    }

    if ($number_of_records > $rows) {
        $pagination = Paginate(FORUM_URL . 'feed.php', $page, $number_of_pages, false, 3);
        $layout->AddContentById('pagination', $pagination);
    }

} else {
    $rows_html = '<p>{{ST:there_are_no_posts}}</p>';
}


$layout->AddContentById('rows', $rows_html);

$layout->RenderViewAndExit();
