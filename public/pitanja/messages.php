<?php

require 'includes.php';

if(!Users_IsUserLoggedIn()){
	Leave(FORUM_URL);
}

$layout = GetPage('messages', '{{ST:messages}}');

$layout->AddContentById('breadcrumbs', ' <li><a href="'.FORUM_URL.'">{{ST:home}}</a></li><li class="active">{{ST:messages}}</li>');



if(isset($_GET['page'])){
	$page = intval($_GET['page']);
}else{
	$page = 1;
}

$rows = ROWS_PER_PAGE;
$offset = ($page - 1) * $rows;

/*
 * SELECT M.* FROM " . TABLES_PREFIX . "messages M INNER JOIN (SELECT contact_id, max(id) as maxId FROM " . TABLES_PREFIX . "messages WHERE owner_id = ".Users_CurrentUserId()." GROUP BY contact_id)T ON M.id = T.maxId ORDER BY M.date_sent DESC LIMIT $offset, $rows
 */


$messages = $db->get_results("SELECT M.* FROM " . TABLES_PREFIX . "messages M INNER JOIN (SELECT contact_id, max(id) as maxId FROM " . TABLES_PREFIX . "messages WHERE owner_id = ".Users_CurrentUserId()." GROUP BY contact_id)T ON M.id = T.maxId ORDER BY M.date_sent DESC LIMIT $offset, $rows");
$number_of_records = count($db->get_results("SELECT M.* FROM " . TABLES_PREFIX . "messages M INNER JOIN (SELECT contact_id, max(id) as maxId FROM " . TABLES_PREFIX . "messages WHERE owner_id = ".Users_CurrentUserId()." GROUP BY contact_id)T ON M.id = T.maxId"));

$number_of_pages = ceil( $number_of_records / $rows );

$my_details = Users_GetUserDetails(Users_CurrentUserId());

$rows_html = '';
if($messages){
	foreach($messages as $m){
			$row_layout = new Layout('html/','str/');

            $row_layout->SetContentView('messages-rows');
			$row_layout->AddContentById('id', $m->id);
		
			$row_layout->AddContentById('message', TrimText($m->message, 80));
			
			$row_layout->AddContentById('date', getRelativeTime($m->date_sent));

            if($m->seen == 'y'){
                $row_layout->AddContentById('is_seen', ' class="active"');
            }
		
			
			$row_layout->AddContentById('contact_id', $m->contact_id);
		
			$user_details = Users_GetUserDetails($m->contact_id);
			if($user_details){
				if($user_details['username']){
					$row_layout->AddContentById('username', $user_details['username']);
				}
                if($user_details['is_admin'] == true){
                    $row_layout->AddContentById('is_admin', '<img title="{{ST:is_admin}}" data-toggle="tooltip" src="{{ID:base_url}}img/admin.png">');
                }elseif($user_details['is_moderator'] == true){
                    $row_layout->AddContentById('is_admin', '<img title="{{ST:moderator}}" data-toggle="tooltip" src="{{ID:base_url}}img/moderator.png">');
                }
				if($user_details['path_to_profile']){
					$row_layout->AddContentById('path_to_profile', $user_details['path_to_profile']);
				}
				if($user_details['path_to_photo']){
					$row_layout->AddContentById('user_photo', $user_details['path_to_photo']);
				}else{
					$row_layout->AddContentById('user_photo', FORUM_URL.'img/anon.png');
				}
			}
		
			$rows_html .= $row_layout->ReturnView();
		}
	
	if($number_of_records>$rows){
        $pagination = Paginate(FORUM_URL.'messages.php', $page, $number_of_pages, false, 3);
        $layout->AddContentById('pagination', $pagination);
	}
	
}else{
    $rows_html = '<tr><td colspan="3">{{ST:no_messages}}</td></tr>';
}

$layout->AddContentById('rows', $rows_html);
$layout->RenderViewAndExit();
