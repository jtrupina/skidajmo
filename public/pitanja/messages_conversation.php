<?php

require 'includes.php';

if (!Users_IsUserLoggedIn()) {
    Leave(FORUM_URL);
}

if (isset($_GET['contact'])) {
    $contact = Users_GetUserDetails(intval($_GET['contact']));
}

$layout = GetPage('messages-conversation', $contact['username']);


$layout->AddContentById('breadcrumbs', ' <li><a href="' . FORUM_URL . '">{{ST:home}}</a></li><li class="active"><a href="' . FORUM_URL . 'messages.php">{{ST:messages}}</a></li><li class="active">' . $contact['username'] . '</li>');

$layout->AddContentById('contact_id', intval($_GET['contact']));
$layout->AddContentById('contact_user_name', $contact['username']);

$sender = Users_CurrentUserId();
$sender_details = Users_GetUserDetails($sender);
$sender_details['username'] = '{{ST:me}}';
$sender_badges = Users_GetUserBadges($sender);
$contact_details = Users_GetUserDetails(intval($_GET['contact']));
$contact_badges = Users_GetUserBadges(intval($_GET['contact']));

if(isset($_GET['delete'])){
    $db->query("DELETE FROM " . TABLES_PREFIX . "messages WHERE id = " . intval($_GET['message']));
    $layout->AddContentById('alert', $layout->GetContent('alert'));
    $layout->AddContentById('alert_nature', ' alert-success');
    $layout->AddContentById('alert_heading', '{{ST:success}}!');
    $layout->AddContentById('alert_message', '{{ST:the_message_has_been_deleted}}');
}

if(isset($_GET['msg']) AND $_GET['msg'] == 'sent'){
    $layout->AddContentById('alert', $layout->GetContent('alert'));
    $layout->AddContentById('alert_nature', ' alert-success');
    $layout->AddContentById('alert_heading', '{{ST:success}}!');
    $layout->AddContentById('alert_message', '{{ST:your_message_has_been_sent}}');
}

if (isset($_POST['send'])) {

    $errors = false;
    $values = array();
    $format = array();
    $error_msg = '';

    if (isset($_POST['message']) AND $_POST['message'] != '') {
        $layout->AddContentById('message', stripcslashes($_POST['message']));
        $message = $_POST['message'];
    } else {
        $errors = true;
        $error_msg .= '{{ST:message_required}} ';
    }

    if (!$errors) {


        $date_sent = date('Y-m-d H:i:s');

        $sending = array('contact_id' => intval($_GET['contact']), 'owner_id' => $sender, 'seen' => 'y', 'message' => $message, 'date_sent' => $date_sent, 'sender' => $sender);
        $receiving = array('contact_id' => $sender, 'owner_id' => intval($_GET['contact']), 'seen' => 'n', 'message' => $message, 'date_sent' => $date_sent, 'sender' => $sender);

        $format = array("%d", "%d", "%s", "%s", "%s", "%d");

        $db->insert(TABLES_PREFIX . "messages", $sending, $format);
        $db->insert(TABLES_PREFIX . "messages", $receiving, $format);

        NewMsgNotification($sender, intval($_GET['contact']));

        $layout->AddContentById('alert', $layout->GetContent('alert'));
        $layout->AddContentById('alert_nature', ' alert-success');
        $layout->AddContentById('alert_heading', '{{ST:success}}!');
        $layout->AddContentById('alert_message', '{{ST:your_message_has_been_sent}}');
    } else {
        $layout->AddContentById('alert', $layout->GetContent('alert'));
        $layout->AddContentById('alert_nature', ' alert-danger');
        $layout->AddContentById('alert_heading', '{{ST:error}}!');
        $layout->AddContentById('alert_message', $error_msg);
    }
}

$rows = ROWS_PER_PAGE;
$number_of_records = count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "messages WHERE contact_id = " . intval($_GET['contact']) . " AND owner_id = " . $sender . ""));
$number_of_pages = ceil($number_of_records / $rows);
if (isset($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}
$offset = ($page - 1) * $rows;

$latest = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "messages WHERE contact_id = " . intval($_GET['contact']) . " AND owner_id = " . $sender . " ORDER BY date_sent DESC LIMIT $offset, $rows");

$rows_html = '';
if ($latest) {
    foreach ($latest as $post) {
        $row_layout = new Layout('html/', 'str/');
        $row_layout->SetContentView('messages-conversation-row');
        $row_layout->AddContentById('id', $post->id);
        $row_layout->AddContentById('contact_id', intval($_GET['contact']));
        $row_layout->AddContentById('page', $page);

        $row_layout->AddContentById('date', getRelativeTime($post->date_sent));
        $row_layout->AddContentById('message', stripcslashes($post->message));

        if($post->sender == $sender){
            $user_details = $sender_details;
            $row_layout->AddContentById('user_badges', $sender_badges);

            if(!$sender_badges){
                $row_layout->AddContentById('user_badges', '');
                $row_layout->AddContentById('display_badges', 'style="display: none;"');
            }else{
                $row_layout->AddContentById('user_badges', $sender_badges);
            }
        }else{
            $user_details = $contact_details;
            $row_layout->AddContentById('user_badges', $contact_badges);

            if(!$contact_badges){
                $row_layout->AddContentById('user_badges', '');
                $row_layout->AddContentById('display_badges', 'style="display: none;"');
            }else{
                $row_layout->AddContentById('user_badges', $contact_badges);
            }
        }

        if ($user_details) {
            if($user_details['is_admin'] == true){
                $row_layout->AddContentById('is_admin', '<img title="{{ST:is_admin}}" data-toggle="tooltip" src="{{ID:base_url}}img/admin.png">');
            }elseif($user_details['is_moderator'] == true){
                $row_layout->AddContentById('is_admin', '<img title="{{ST:moderator}}" data-toggle="tooltip" src="{{ID:base_url}}img/moderator.png">');
            }
            if ($user_details['username']) {
                $row_layout->AddContentById('user_name', $user_details['username']);
            }
            if ($user_details['path_to_profile']) {
                $row_layout->AddContentById('path_to_profile', $user_details['path_to_profile']);
            }
            if ($user_details['path_to_photo']) {
                $row_layout->AddContentById('user_photo', $user_details['path_to_photo']);
            } else {
                $row_layout->AddContentById('user_photo', FORUM_URL . 'img/anon.png');
            }
        }

        $rows_html .= $row_layout->ReturnView();
    }

    if ($number_of_records > $rows) {
        $pagination = Paginate(FORUM_URL . 'messages_conversation.php?contact=' . intval($_GET['contact']), $page, $number_of_pages, true, 3);
        $layout->AddContentById('pagination', $pagination);
    }

} else {
    $rows_html = '<p>{{ST:there_are_no_messages}}</p>';
}


$layout->AddContentById('rows', $rows_html);


$db->update(TABLES_PREFIX . "messages", array('seen' => 'y'), array('owner_id' => $sender, 'contact_id'=>intval($_GET['contact'])), array("%s"));


$layout->RenderViewAndExit();