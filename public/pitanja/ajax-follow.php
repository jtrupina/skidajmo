<?php

require 'includes.php';

if (!Users_IsUserLoggedIn()) {
    exit();
}

if(isset($_GET['follow']) AND intval($_GET['follow']) == 1 AND Users_IsUserLoggedIn()){
    $db->insert(TABLES_PREFIX . "following", array('follower_id'=>Users_CurrentUserId(), 'user_id'=>intval($_GET['id'])), array("%d","%d"));
}elseif(isset($_GET['unfollow']) AND intval($_GET['unfollow']) == 1 AND Users_IsUserLoggedIn()){
    $db->query("DELETE FROM " . TABLES_PREFIX . "following WHERE user_id = ".intval($_GET['id'])." AND follower_id = ".Users_CurrentUserId());
}

exit();