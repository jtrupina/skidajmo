<?php

function Users_LoggedIn($user = null, $remember_me = false){
    $_SESSION[INSTALLATION_KEY.'sforum_logged_in'] = true;
    $_SESSION[INSTALLATION_KEY.'sforum_user_id'] = $user->id;
    $_SESSION[INSTALLATION_KEY.'sforum_user_role'] = $user->role;
    $_SESSION[INSTALLATION_KEY.'sforum_user_username'] = $user->username;
    if($remember_me){
        setcookie(INSTALLATION_KEY . 'SimpleForumCookie', 'email='.$user->username.'&hash='.$user->password, time() + 10368000);
    }
}

function Users_LoggedOut(){
    unset($_SESSION[INSTALLATION_KEY.'sforum_logged_in']);
    unset($_SESSION[INSTALLATION_KEY.'sforum_user_id']);
    unset($_SESSION[INSTALLATION_KEY.'sforum_user_role']);
    unset($_SESSION[INSTALLATION_KEY.'sforum_user_username']);

    setcookie(INSTALLATION_KEY . 'SimpleForumCookie', "", time() - 3600);
}

//Check if there is an active session
//Return boolean
function Users_IsUserLoggedIn(){
	$db = Db::GetInstance();
	
	if(isset($_SESSION[INSTALLATION_KEY.'sforum_logged_in']) AND ($_SESSION[INSTALLATION_KEY.'sforum_logged_in'] == true)){
		$db->update(TABLES_PREFIX . "users", array('last_seen'=> date('Y-m-d H:i:s')), array('id'=>intval($_SESSION[INSTALLATION_KEY.'sforum_user_id'])), array("%s"));
		return true;
	}else{
		if(isset($_COOKIE[INSTALLATION_KEY . 'SimpleForumCookie'])) {
			parse_str($_COOKIE[INSTALLATION_KEY . 'SimpleForumCookie']);
			$user_details = Clean(array('email'=>$email,'hash'=>$hash));
			$user = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "users WHERE email = '" . $user_details['email'] . "'  OR username = '" . $user_details['email'] . "' ORDER BY id DESC LIMIT 0,1");
			if($user AND ($user->password == $user_details['hash'])){
				if($user->status == 'pending'){
					return false;
				}elseif($user->status == 'banned'){
					return false;
				}else{
                    setcookie(INSTALLATION_KEY . 'SimpleForumCookie', 'email='.$user->username.'&hash='.$user->password, time() + 10368000);

                    $_SESSION[INSTALLATION_KEY.'sforum_logged_in'] = true;
					$_SESSION[INSTALLATION_KEY.'sforum_user_id'] = $user->id;
					$_SESSION[INSTALLATION_KEY.'sforum_user_role'] = $user->role;
					$_SESSION[INSTALLATION_KEY.'sforum_user_username'] = $user->username;

                    $db->update(TABLES_PREFIX . "users", array('last_seen'=> date('Y-m-d H:i:s')), array('id'=>intval($_SESSION[INSTALLATION_KEY.'sforum_user_id'])), array("%s"));
					return true;
				}
			}
		}
		
	}
	return false;
}

//Gets the current user's ID
//Return an integer
function Users_CurrentUserId(){
    if(isset($_SESSION[INSTALLATION_KEY.'sforum_user_id'])){
        return intval($_SESSION[INSTALLATION_KEY.'sforum_user_id']);
    }else{
        return null;
    }
}

//Gets the current user's username
//Return an string
function Users_CurrentUserUsername(){
    if(isset($_SESSION[INSTALLATION_KEY.'sforum_user_username'])){
        return $_SESSION[INSTALLATION_KEY.'sforum_user_username'];
    }else{
        return '';
    }
}


//Checks if the user with the $id is an administrator
//Return boolean
function Users_IsUserAdmin($id){
	if(!$id)
		return false;

    if($id == Users_CurrentUserId()){
        if($_SESSION[INSTALLATION_KEY.'sforum_user_role'] == 'admin'){
            return true;
        }else{
            false;
        }
    }
	
	$role = helper_get_role($id);

	if($role == 'admin'){
		return true;
	}else{
		return false;
	}
}

//Checks if the user with the $id is a moderator
//Return boolean
function Users_IsUserModerator($id){
	if(!$id)
        return false;

    if($id == Users_CurrentUserId()){
        if($_SESSION[INSTALLATION_KEY.'sforum_user_role'] == 'moderator'){
            return true;
        }else{
            false;
        }
    }

    $role = helper_get_role($id);

    if($role == 'moderator'){
        return true;
    }else{
        return false;
    }
}

function Users_IsUserAdminOrModerator($id){
    if(!$id)
        return false;

    if($id == Users_CurrentUserId()){
        if($_SESSION[INSTALLATION_KEY.'sforum_user_role'] == 'moderator' OR $_SESSION[INSTALLATION_KEY.'sforum_user_role'] == 'admin'){
            return true;
        }else{
            false;
        }
    }

    $role = helper_get_role($id);

    if($role == 'moderator' OR $role == 'admin'){
        return true;
    }else{
        return false;
    }
}

//Check if the current user is able to start threads and reply to them
//Return boolean
function Users_CanCurrentUserPost(){
	if(!Users_CurrentUserId())
		return false;
	$id = intval(Users_CurrentUserId());

    $db = Db::GetInstance();
	$status = $db->get_var("SELECT status FROM " . TABLES_PREFIX . "users WHERE id = $id ORDER BY id DESC LIMIT 0,1");


	if($status == 'active'){
		return true;
	}else{
		return false;
	}
}

function Users_username_search($q){
    $db = Db::GetInstance();
    $admins = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "users WHERE username LIKE '$q%' ORDER BY username ASC");

    $return = array();
    if($admins){
        foreach($admins as $admin){
            $return[] = $admin->username;
        }
    }

    return $return;
}



//Gets the details of the user with the $id
//path_to_photo and bio are optional. Everything else is required.
//Return an array: array('id'=>[int],'username'=>[string],'email'=>[string],'path_to_photo'=>[string],'is_admin'=>[bool],'bio'=>[string])
function Users_GetUserDetails($id){
	$return = array('id'=>null,'username'=>'','email'=>'','path_to_photo'=>'','is_admin'=>false,'bio'=>'','signature'=>'');

	$user = helper_get_user_by_id($id);
	if($user){
		$return['id'] = $user->id;
		$return['username'] = $user->username;
		$return['email'] = $user->email;
		if($user->role == 'admin'){
			$return['is_admin'] = true;
		}else{
			$return['is_admin'] = false;
		}
		if($user->role == 'moderator'){
			$return['is_moderator'] = true;
		}else{
			$return['is_moderator'] = false;
		}
		if($user->photo){
			$return['path_to_photo'] = FORUM_URL . $user->photo;
		}
		$return['bio'] = $user->bio;
		$return['signature'] = $user->signature;
		$return['path_to_profile'] = FORUM_URL . 'profiles.php?id='.$user->id;
	}
	return $return;
}

//Gets the details of the user with the $id
//path_to_photo and bio are optional. Everything else is required.
//Return an array: array('id'=>[int],'username'=>[string],'email'=>[string],'path_to_photo'=>[string],'is_admin'=>[bool],'bio'=>[string])
function Users_GetUserDetails_byUsername($username){
    $return = array('id'=>null,'username'=>'','email'=>'','path_to_photo'=>'','is_admin'=>false,'bio'=>'','signature'=>'');
    $user = helper_get_user_by_username($username);
    if($user){
        $return['id'] = $user->id;
        $return['username'] = $user->username;
        $return['email'] = $user->email;
        if($user->role == 'admin'){
            $return['is_admin'] = true;
        }else{
            $return['is_admin'] = false;
        }
        if($user->role == 'moderator'){
            $return['is_moderator'] = true;
        }else{
            $return['is_moderator'] = false;
        }
        if($user->photo){
            $return['path_to_photo'] = FORUM_URL . $user->photo;
        }
        $return['bio'] = $user->bio;
        $return['signature'] = $user->signature;
        $return['path_to_profile'] = FORUM_URL . 'profiles.php?id='.$user->id;
    }else{
        return false;
    }
    return $return;
}

//Get the html for displaying user badges/achievements
function Users_GetUserBadges($id){
    $db = Db::GetInstance();
	$badges_html = '';
	$all_posts_count = count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts WHERE user_id =".intval($id) ));
	$started_threads_count = count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts WHERE is_question = 'y' AND user_id =".intval($id) ));
	$replies_count = $all_posts_count - $started_threads_count;


	$months_query = $db->get_var("SELECT added_on FROM " . TABLES_PREFIX . "users WHERE id = " . intval($id));
	if($months_query){
		$months_count = intval(floor(abs(time() - strtotime($months_query))/(60*60*24*30)));
	}else{
		$months_count = 0;
	}
	
	$achievements_query = $db->get_var("SELECT achievements FROM " . TABLES_PREFIX . "users WHERE id = " . intval($id));
	if($achievements_query){
		$achievements_array =  unserialize($achievements_query);
		$manual_achievements = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "achievements WHERE id IN ( ". implode(",", $achievements_array) .")");
		if($manual_achievements){
			foreach($manual_achievements as $b){
				$badges_html .= '<img src="'.FORUM_URL.'php/timthumb.php?src='.FORUM_URL.$b->icon.'&w=24&h=24" title="' . $b->name . '"/>&nbsp;';
			}
		}
	}
	
	$all_posts_badge = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "achievements WHERE type = 'all_posts' AND start_from <= $all_posts_count AND end_at >= $all_posts_count");
	$started_threads_badge = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "achievements WHERE type = 'started_threads' AND start_from <= $started_threads_count AND end_at >= $started_threads_count");
	$replies_badge = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "achievements WHERE type = 'replies' AND start_from <= $replies_count AND end_at >= $replies_count");
	
	$months_badge = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "achievements WHERE type = 'membership_time' AND start_from <= $months_count AND end_at >= $months_count");
	
	if($all_posts_badge){
		foreach($all_posts_badge as $b){
			$badges_html .= '<img src="'.FORUM_URL.'php/timthumb.php?src='.FORUM_URL.$b->icon.'&w=24&h=24" title="' . $b->name . '"/>&nbsp;';
		}
	}
	
	if($started_threads_badge){
		foreach($started_threads_badge as $b){
			$badges_html .= '<img src="'.FORUM_URL.'php/timthumb.php?src='.FORUM_URL.$b->icon.'&w=24&h=24" title="' . $b->name . '"/>&nbsp;';
		}
	}
	
	if($replies_badge){
		foreach($replies_badge as $b){
			$badges_html .= '<img src="'.FORUM_URL.'php/timthumb.php?src='.FORUM_URL.$b->icon.'&w=24&h=24" title="' . $b->name . '"/>&nbsp;';
		}
	}
	if($months_badge){
		foreach($months_badge as $b){
			$badges_html .= '<img src="'.FORUM_URL.'php/timthumb.php?src='.FORUM_URL.$b->icon.'&w=24&h=24" title="' . $b->name . '"/>&nbsp;';
		}
	}
	return $badges_html;
}

//The page to direct users to sign in
//Return an string
function Users_SignInPageUrl(){
	return FORUM_URL . 'signin.php';
}

//The page to direct users to sign pout
//Return an string
function Users_SignOutPageUrl(){
	if(isset($_SESSION[INSTALLATION_KEY.'sforum_logout_link']) AND $_SESSION[INSTALLATION_KEY.'sforum_logout_link'] != ''){
		return $_SESSION[INSTALLATION_KEY.'sforum_logout_link'];
	}else{
		return FORUM_URL . 'signout.php';
	}
}

//The page to direct users to sign up
//Return an string
function Users_RegistrationPageUrl(){
	return FORUM_URL . 'signup.php';
}

//The page to direct users when they have forgotten their password
//Return an string
function Users_ForgotPasswordPageUrl(){
	return FORUM_URL . 'forgot.php';
}

//The page to direct users to change their details
//Return an string
function Users_ChangeProfilePageUrlCurrentUser(){
	return FORUM_URL . 'profile.php';
}

/*
 * DON'T MODIFY FROM HERE ONWARDS
 */

function helper_get_role($id){
    $db = Db::GetInstance();

    $cache = new Cache;

    $results = $cache->read("SELECT role FROM " . TABLES_PREFIX . "users WHERE id = $id");

    if(!$results){
        $results = $db->get_var("SELECT role FROM " . TABLES_PREFIX . "users WHERE id = $id");
        if($results){
            $cache->write("SELECT role FROM " . TABLES_PREFIX . "users WHERE id = $id", $results);
        }
    }

    return $results;
}

function helper_get_user_by_id($id){
    $db = Db::GetInstance();

    $cache = new Cache;

    $results = $cache->read("SELECT id, username, email, status , likes, role, notes, hash, photo, bio, facebook_id, added_on, achievements, last_seen, website_url, facebook_url, twitter_url, google_url, signature FROM " . TABLES_PREFIX . "users WHERE id = $id ORDER BY id DESC LIMIT 0,1");

    if(!$results){
        $results = $db->get_row("SELECT id, username, email, status , likes, role, notes, hash, photo, bio, facebook_id, added_on, achievements, last_seen, website_url, facebook_url, twitter_url, google_url, signature FROM " . TABLES_PREFIX . "users WHERE id = $id ORDER BY id DESC LIMIT 0,1");
        if($results){
            $cache->write("SELECT id, username, email, status , likes, role, notes, hash, photo, bio, facebook_id, added_on, achievements, last_seen, website_url, facebook_url, twitter_url, google_url, signature FROM " . TABLES_PREFIX . "users WHERE id = $id ORDER BY id DESC LIMIT 0,1", $results);
        }
    }

    $user = $results;

    if($user){
        return $user;
    }else{
        return false;
    }
}

function helper_get_user_by_username($username){
    $db = Db::GetInstance();

    $cache = new Cache;

    $results = $cache->read("SELECT id, username, email, status , likes, role, notes, hash, photo, bio, facebook_id, added_on, achievements, last_seen, website_url, facebook_url, twitter_url, google_url, signature FROM " . TABLES_PREFIX . "users WHERE username = '$username' ORDER BY id DESC LIMIT 0,1");

    if(!$results){
        $results = $db->get_row("SELECT id, username, email, status , likes, role, notes, hash, photo, bio, facebook_id, added_on, achievements, last_seen, website_url, facebook_url, twitter_url, google_url, signature FROM " . TABLES_PREFIX . "users WHERE username = '$username' ORDER BY id DESC LIMIT 0,1");
        if($results){
            $cache->write("SELECT id, username, email, status , likes, role, notes, hash, photo, bio, facebook_id, added_on, achievements, last_seen, website_url, facebook_url, twitter_url, google_url, signature FROM " . TABLES_PREFIX . "users WHERE username = '$username' ORDER BY id DESC LIMIT 0,1", $results);
        }
    }

    $user = $results;

    if($user){
        return $user;
    }else{
        return false;
    }
}
