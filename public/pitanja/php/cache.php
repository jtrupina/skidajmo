<?php


class Cache {

    var $path_to_cache = '';
    var $salt = '';

    function __construct() {
        $this->path_to_cache = BASE_PATH . 'cache/';
        $this->salt = @filemtime(__FILE__) . '-' . @fileinode(__FILE__);
        $this->cleanCache();
    }

    function query_to_filename($query){
        return '/home/jtrupina/PhpstormProjects/skidajmo/public/pitanja/cache/'.md5($this->salt . $query) . 'cache.txt';
    }

    function read($query) {
        if(!CACHE_USER_API_RESPONSES){
            return null;
        }

        $fileName = $this->query_to_filename($query);
        if (file_exists($fileName)) {
            if( filemtime( $fileName ) > ( time() - CACHE_USER_API_MAX_AGE_SECONDS ) )
            {
                return unserialize( file_get_contents( $fileName ) );
            }else{
                $this->delete($query);
                return null;
            }
        } else {
            return null;
        }
    }


    function write($query,$variable) {
        $fileName = $this->query_to_filename($query);
        file_put_contents( $fileName, serialize( $variable ) );
    }


    function delete($query) {
        $fileName = $this->query_to_filename($query);
        @unlink($fileName);
    }

    protected function cleanCache(){
        if (CACHE_USER_API_MAX_AGE_SECONDS < 1) {
            return null;
        }

        $lastCleanFile = $this->path_to_cache . 'cacheLastCleanTime.touch';

        if(! is_file($lastCleanFile)){
            @touch($lastCleanFile);
            return null;
        }
        if(@filemtime($lastCleanFile) < (time() - CACHE_USER_API_MAX_AGE_SECONDS) ){
            @touch($lastCleanFile);
            $files = glob($this->path_to_cache . '*' . 'cache.txt');
            if ($files) {
                $timeAgo = time() - CACHE_USER_API_MAX_AGE_SECONDS;
                foreach($files as $file){
                    if(@filemtime($file) < $timeAgo){
                        @unlink($file);
                    }
                }
            }
            return true;
        }
        return false;
    }

}