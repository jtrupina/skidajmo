<?php
/** VERSION: 4 */

define('DB_NAME', 'skidajmo_pitanja');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'nosurr37');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables and in the database connection. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('TABLES_PREFIX', 'sforum_');

/** Set to false when in production to suppress errors for users. */
define('DEVELOPMENT', true);

/** Webmaster's email */
define('WEBMASTER_EMAIL', 'ikutnjak@gmail.com');

define('ALLOWED_FILE_TYPES', 'gif|jpg|png|pdf|doc|docx|txt|xls');

define('CACHE_USER_API_RESPONSES', true);
define('CACHE_USER_API_MAX_AGE_SECONDS', 86400);


/** Number of times a post should be flagged before it is hidden. */
define('HIDE_POST_ON_THIS_NUMBER_OF_FLAGS', 10);


/** When a new private message is sent, should an email notification be sent to the recipient? */
define('SEND_EMAIL_NOTIFICATION_FOR_NEW_PM', true);

/** Bootstrap Themes:
Courtesy of http://bootswatch.com/
License: Apache License v2.0
NB: All the themes except for "default" use google fonts
*/
