<?php

require 'includes.php';

if (!Users_IsUserLoggedIn()) {
    echo '';
    exit();
}

if (isset($_GET['contact'])) {
    $contact = Users_GetUserDetails(intval($_GET['contact']));
}

$sender = Users_CurrentUserId();
$sender_details = Users_GetUserDetails($sender);
$sender_details['username'] = '{{ST:me}}';
$sender_badges = Users_GetUserBadges($sender);
$contact_details = Users_GetUserDetails(intval($_GET['contact']));
$contact_badges = Users_GetUserBadges(intval($_GET['contact']));

$latest = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "messages WHERE seen = 'n' AND contact_id = " . intval($_GET['contact']) . " AND owner_id = " . $sender . " ORDER BY date_sent DESC");

$rows_html = '';
if ($latest) {
    foreach ($latest as $post) {
        $row_layout = new Layout('html/', 'str/');
        $row_layout->SetContentView('messages-conversation-row');
        $row_layout->AddContentById('id', $post->id);
        $row_layout->AddContentById('contact_id', intval($_GET['contact']));
        $row_layout->AddContentById('page', 1);

        $row_layout->AddContentById('date', getRelativeTime($post->date_sent));
        $row_layout->AddContentById('message', stripcslashes($post->message));

        if($post->sender == $sender){
            $user_details = $sender_details;
            $row_layout->AddContentById('user_badges', $sender_badges);
        }else{
            $user_details = $contact_details;
            $row_layout->AddContentById('user_badges', $contact_badges);
        }

        if ($user_details) {
            if ($user_details['is_admin'] == true) {
                $row_layout->AddContentById('is_admin', '<span class="label label-warning">{{ST:is_admin}}</span>');
            } elseif ($user_details['is_moderator'] == true) {
                $row_layout->AddContentById('is_admin', '<span class="label label-info">{{ST:moderator}}</span>');
            }
            if ($user_details['username']) {
                $row_layout->AddContentById('user_name', $user_details['username']);
            }
            if ($user_details['path_to_profile']) {
                $row_layout->AddContentById('path_to_profile', $user_details['path_to_profile']);
            }
            if ($user_details['path_to_photo']) {
                $row_layout->AddContentById('user_photo', $user_details['path_to_photo']);
            } else {
                $row_layout->AddContentById('user_photo', FORUM_URL . 'img/anon.png');
            }
        }

        $rows_html .= $row_layout->ReturnView();
    }

}


$db->update(TABLES_PREFIX . "messages", array('seen' => 'y'), array('owner_id' => $sender, 'contact_id'=>intval($_GET['contact'])), array("%s"));


echo $rows_html;
exit();