<?php

require 'includes.php';

if(!Users_IsUserLoggedIn()){
    Leave(FORUM_URL);
}

$layout = GetPage('messages-new', '{{ST:new_message}}');

$layout->AddContentById('breadcrumbs', ' <li><a href="'.FORUM_URL.'">{{ST:home}}</a></li><li class="active"><a href="'.FORUM_URL.'messages.php">{{ST:messages}}</a></li><li class="active">{{ST:new_message}}</li>');

$admins = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "users WHERE id != " . Users_CurrentUserId() . " ORDER BY username ASC");

$user_names = [];
if($admins){
    foreach($admins as $admin){
        $user_names[] = '"'.$admin->username.'"';
    }
}

if(isset($_GET['contact'])){
    $contact = Users_GetUserDetails(intval($_GET['contact']));
    if($contact['username']){
        $layout->AddContentById('receiver', $contact['username']);
    }
}

if(isset($_POST['send'])){

    $errors = false;
    $values = array();
    $format = array();
    $error_msg = '';

    if(isset($_POST['message']) AND $_POST['message'] != ''){
        $layout->AddContentById('message', stripcslashes($_POST['message']));
        $message = $_POST['message'];
    }else{
        $errors = true;
        $error_msg .= '{{ST:message_required}} ';
    }

    if(isset($_POST['receiver']) AND $_POST['receiver'] != ''){
        $layout->AddContentById('receiver', $_POST['receiver']);
        $receiver = Users_GetUserDetails_byUsername($_POST['receiver']);

        if($receiver){
            if(Users_CurrentUserId() == $receiver['id']){
                $errors = true;
                $error_msg .= '{{ST:cant_send_yourself_a_message}} ';
            }
        }else{
            $errors = true;
            $error_msg .= '{{ST:username_doesnt_exist}} ';
        }


    }else{
        $errors = true;
        $error_msg .= '{{ST:receiver_required}} ';
    }

    if(!$errors){
        $sender = Users_CurrentUserId();

        $date_sent = date('Y-m-d H:i:s');

        $sending = array('contact_id'=>$receiver['id'],'owner_id'=>$sender,'seen'=>'y','message'=>$message,'date_sent'=>$date_sent,'sender'=>$sender);
        $receiving = array('contact_id'=>$sender,'owner_id'=>$receiver['id'],'seen'=>'n','message'=>$message,'date_sent'=>$date_sent,'sender'=>$sender);

        $format = array("%d","%d","%s","%s","%s","%d");

        $db->insert(TABLES_PREFIX . "messages", $sending, $format);
        $db->insert(TABLES_PREFIX . "messages", $receiving, $format);

        NewMsgNotification($sender, $receiver['id']);

        $layout->AddContentById('alert', $layout->GetContent('alert'));
        $layout->AddContentById('alert_nature', ' alert-success');
        $layout->AddContentById('alert_heading', '{{ST:success}}!');
        $layout->AddContentById('alert_message', '{{ST:your_message_has_been_sent}}');

        Leave(FORUM_URL . 'messages_conversation.php?msg=sent&contact=' . $receiver['id']);
    }else{
        $layout->AddContentById('alert', $layout->GetContent('alert'));
        $layout->AddContentById('alert_nature', ' alert-danger');
        $layout->AddContentById('alert_heading', '{{ST:error}}!');
        $layout->AddContentById('alert_message', $error_msg);
    }
}


$layout->AddContentById('user_names', '[' . implode(',', $user_names)  . ']');

$layout->RenderViewAndExit();
