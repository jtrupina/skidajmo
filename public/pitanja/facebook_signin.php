<?php

require 'includes.php';
require 'php/facebook/autoload.php';


use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;

FacebookSession::setDefaultApplication(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET);

$session = new FacebookSession($_GET['access_token']);

if ($session) {
    try {

        $me = (new FacebookRequest(
            $session, 'GET', '/me'
        ))->execute()->getGraphObject(GraphUser::className());

        $user_details = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "users WHERE facebook_id = '" . $me->getId() . "' ORDER BY id DESC LIMIT 0,1");
        if($user_details){
            Users_LoggedIn($user_details);
        }else{
            $values = array();
            $format = array();

            $values['email'] = $me->getEmail();
            $format[] = "%s";

            $values['facebook_id'] = $me->getId();
            $format[] = "%s";

            $values['username'] = GenerateUsername($me->getName());
            $format[] = "%s";

            $values['photo'] = GetFacebookPhoto('https://graph.facebook.com/'.$me->getId().'/picture?type=large');
            $format[] = "%s";

            $values['facebook_url'] = $me->getLink();
            $format[] = "%s";

            $values['role'] = 'user';
            $format[] = "%s";

            $values['status'] = 'active';
            $format[] = "%s";

            $values['added_on'] = date('Y-m-d H:i:s');
            $format[] = "%s";

            $values['likes'] = 0;
            $format[] = "%d";

            $db->insert(TABLES_PREFIX . "users", $values, $format);
            $user_details = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "users WHERE id = '" . $db->insert_id . "' ORDER BY id DESC LIMIT 0,1");
            Users_LoggedIn($user_details);
        }
    } catch(FacebookRequestException $e) {
        echo "Exception occured, code: " . $e->getCode();
        echo " with message: " . $e->getMessage();
    }
}


header('Location: ' . $_SERVER['HTTP_REFERER']);
exit();

