<?php

require 'includes.php';

$layout = GetPage('online', '{{ST:who_is_online}}');

$layout->AddContentById('breadcrumbs', ' <li><a href="'.FORUM_URL.'">{{ST:home}}</a></li><li class="active">{{ST:who_is_online}}</li>');

$timecheck = date('Y-m-d H:i:s',time()-(60*5));

$online = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "users WHERE last_seen > '$timecheck'");

$rows_html = '';
if($online){
	foreach($online as $o){
		$row_layout = new Layout('html/','str/');
		$row_layout->SetContentView('online-rows');

        $row_layout->AddContentById('id', $o->id);
		$user_details = Users_GetUserDetails($o->id);
		if($user_details){
            if($user_details['is_admin'] == true){
                $row_layout->AddContentById('is_admin', '<img title="{{ST:is_admin}}" data-toggle="tooltip" src="{{ID:base_url}}img/admin.png">');
            }elseif($user_details['is_moderator'] == true){
                $row_layout->AddContentById('is_admin', '<img title="{{ST:moderator}}" data-toggle="tooltip" src="{{ID:base_url}}img/moderator.png">');
            }
				if($user_details['username']){
					$row_layout->AddContentById('user_name', $user_details['username']);
				}
				if($user_details['path_to_profile']){
					$row_layout->AddContentById('path_to_profile', $user_details['path_to_profile']);
				}
				if($user_details['path_to_photo']){
					$row_layout->AddContentById('user_photo', $user_details['path_to_photo']);
				}else{
					$row_layout->AddContentById('user_photo', FORUM_URL.'img/anon.png');
				}
		}
			
		//$row_layout->AddContentById('user_badges', Users_GetUserBadges($o->id));

        if((Users_CurrentUserId() == $o->id) OR !Users_IsUserLoggedIn()){
            $row_layout->AddContentById('hide_message_button', ' style="display: none;"');

            $row_layout->AddContentById('hide_follow_button', ' style="display: none;"');
            $row_layout->AddContentById('hide_unfollow_button', ' style="display: none;"');

            if(Users_CurrentUserId() != $o->id){
                $row_layout->AddContentById('hide_profile_button', ' style="display: none;"');
            }

        }else{
            $following = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "following WHERE user_id = ".$o->id." AND follower_id = ".Users_CurrentUserId()." ORDER BY id DESC LIMIT 0,1");
            if($following){
                $row_layout->AddContentById('hide_follow_button', ' style="display: none;"');
            }else{
                $row_layout->AddContentById('hide_unfollow_button', ' style="display: none;"');
            }

            if(Users_CurrentUserId() != $o->id){
                $row_layout->AddContentById('hide_profile_button', ' style="display: none;"');
            }
        }
		
		$rows_html .= $row_layout-> ReturnView();
	}
}else{
	$rows_html = '<center>{{ST:no_users_online}}</center>';
}

$layout->AddContentById('rows', $rows_html);

$layout->RenderViewAndExit();
