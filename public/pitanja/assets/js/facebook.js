window.fbAsyncInit = function() {
    FB.init({
        appId      : facebook_app_id,
        cookie     : true,
        xfbml      : true,
        version    : 'v2.1'
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$(document).ready(function(){
    $('#facebook_signin').click(function () {

        FB.login(function(response) {
            if (response.authResponse) {
                var access_token =   FB.getAuthResponse()['accessToken'];
                window.location = base_url + "facebook_signin.php?access_token=" + access_token;
            } else {
                return false;
            }
        }, {scope: 'email'});

        return false;
    });

});