$(document).ready(function(){
    $(function () {
        $('[data-toggle="popover"]').popover()
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });


    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');

});

$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 400; // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = translate_more;
    var lesstext = translate_less;


    $('.more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

            $(this).html(html);
        }

    });

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});

$(document).ready(function() {
    $(".follow_btn").click(function(){
        var the_id = $(this).data('id');
        $('#follow_' + the_id).hide();
        $('#unfollow_' + the_id).show();
        $.get(base_url + 'ajax-follow.php?follow=1&id=' + the_id);
        return false;
    });

    $(".unfollow_btn").click(function(){
        var the_id = $(this).data('id');
        $('#unfollow_' + the_id).hide();
        $('#follow_' + the_id).show();
        $.get(base_url + 'ajax-follow.php?unfollow=1&id=' + the_id);
        return false;
    });
});
