<?php

require 'includes.php';

$output = array('posts'=>'','last_id'=>0);

if (!Users_IsUserLoggedIn()) {
    echo json_encode($output);
    exit;
}

$search = "";
$following = array();
$subscriptions = array();

$following_query = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "following WHERE follower_id =".Users_CurrentUserId()."" );
if($following_query){
    foreach($following_query as $q){
        $following[] = $q->user_id;
    }
}

$subscriptions_query = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts_following WHERE user_id =".Users_CurrentUserId()."" );
if($subscriptions_query){
    foreach($subscriptions_query as $q){
        $subscriptions[] = $q->post_id;
    }
}

if(count($following) > 0 AND count($subscriptions) > 0){
    $search = " AND (user_id IN (".implode(',', $following).") OR parent_id IN (".implode(',', $subscriptions)."))";
}else{
    if(count($following) > 0){
        $search = "AND user_id IN (".implode(',', $following).")";
    }elseif(count($subscriptions) > 0){
        $search = "AND parent_id IN (".implode(',', $subscriptions).")";
    }
}

if(count($following) > 0 OR count($subscriptions) > 0){
    $latest = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id > ".$_GET['last']." AND  approved = 'y' AND flagged = 'n' $search ORDER BY date DESC");
}else{
    $latest = array();
}

$first = true;
$rows_html = '';
if ($latest) {
    foreach ($latest as $post) {
        $row_layout = new Layout('html/', 'str/');
        $row_layout->SetContentView('feed-rows');
        $row_layout->AddContentById('id', $post->id);

        if($first){
            $first = false;
            $output['last_id'] = $post->id;
        }

        $row_layout->AddContentById('page', 1);

        if ($post->is_question == 'y') {
            $question = $post;
        } else {
            $question = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = " . $post->parent_id . " ORDER BY id DESC LIMIT 0,1");
        }

        $row_layout->AddContentById('thread_id', $question->id);

        if ($post->photos != '') {
            $files = unserialize($post->photos);
            if (count($files) > 0 AND is_array($files)) {
                $files_lists = '<br/><p>';
                $files_count = 1;
                foreach ($files as $f) {
                    $files_lists .= '<img src="' . get_file_icon_path($f) . '">&nbsp;<a target="_blank" href="' . FORUM_URL . 'uploads/' . $f . '">{{ST:attachment}} ' . $files_count . '</a>&nbsp;<br/>';
                    $files_count++;
                }
                $files_lists .= '</p>';
                $row_layout->AddContentById('files', $files_lists);
            }
        }


        $signature = "";

        $row_layout->AddContentById('likes', NiceNumber($post->likes));

        $row_layout->AddContentById('date', getRelativeTime($post->date));

        $row_layout->AddContentById('user_id', $post->user_id);

        $user_details = Users_GetUserDetails($post->user_id);
        if ($user_details) {
            if ($user_details['is_admin'] == true) {
                $row_layout->AddContentById('is_admin', '<span class="label label-warning">{{ST:is_admin}}</span>');
            } elseif ($user_details['is_moderator'] == true) {
                $row_layout->AddContentById('is_admin', '<span class="label label-info">{{ST:moderator}}</span>');
            }
            if ($user_details['username']) {
                $row_layout->AddContentById('user_name', $user_details['username']);
            }
            if ($user_details['path_to_profile']) {
                $row_layout->AddContentById('path_to_profile', $user_details['path_to_profile']);
            }
            if ($user_details['path_to_photo']) {
                $row_layout->AddContentById('user_photo', $user_details['path_to_photo']);
            } else {
                $row_layout->AddContentById('user_photo', FORUM_URL . 'img/anon.png');
            }

            if ($user_details['signature']) {
                $signature = "<hr/><div class='muted'>" . $user_details['signature'] . "</div>";

                $row_layout->AddContentById('signature', $signature);
            }
        }

        if (defined('SEO_HUMAN_FRIENDLY_URLS') AND SEO_HUMAN_FRIENDLY_URLS == true) {
            $thread_url = '{{ID:base_url}}thread/' . UrlText($post->title) . '/' . $post->id . '/';
        } else {
            $thread_url = '{{ID:base_url}}thread.php?id=' . $post->id;
        }

        if ($post->quote AND $post->quote != '') {
            $row_layout->AddContentById('question', '<h4>{{ST:thread_title}}: <a href="' . $thread_url . '">' . $question->title . '</a></h4>' . $post->quote . $post->body);
        } else {
            $row_layout->AddContentById('question', '<h4>{{ST:thread_title}}: <a href="' . $thread_url . '">' . $question->title . '</a></h4>' . $post->body);
        }

        $row_layout->AddContentById('user_badges', Users_GetUserBadges($post->user_id));

        if (!Users_IsUserAdminOrModerator(Users_CurrentUserId())) {
            if (Users_CurrentUserId() != intval($post->user_id)) {
                $row_layout->AddContentById('edit_state', 'style="display:none"');
                $row_layout->AddContentById('delete_state', 'style="display:none"');
            }
        }

        if (!Users_IsUserLoggedIn()) {
            $row_layout->AddContentById('like_or_un_get', 'like');
            $row_layout->AddContentById('like_or_un', 'btn-default');
            $row_layout->AddContentById('like_or_un_text', '{{ST:like}}');
            $row_layout->AddContentById('like_alert', 'onclick="return SignInAlert();"');
            $row_layout->AddContentById('flag_alert', 'onclick="return SignInAlert();"');
        } elseif (count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "likes WHERE user_id = " . intval(Users_CurrentUserId()) . " AND post_id = " . intval($post->id) . "")) > 0) {

            $row_layout->AddContentById('like_or_un_get', 'unlike');
            $row_layout->AddContentById('like_or_un', 'btn-success');
            $row_layout->AddContentById('like_or_un_text', '{{ST:unlike}}');
            $row_layout->AddContentById('flag_alert', 'onclick="return confirm(\'{{ST:are_you_sure}}\');"');
        } else {
            $row_layout->AddContentById('like_or_un_get', 'like');
            $row_layout->AddContentById('like_or_un', 'btn-default');
            $row_layout->AddContentById('like_or_un_text', '{{ST:like}}');
            $row_layout->AddContentById('flag_alert', 'onclick="return confirm(\'{{ST:are_you_sure}}\');"');
        }

        if ($question->locked == 'y') {
            $row_layout->AddContentById('post_alert', 'onclick="return LockedAlert();"');
        }


        $rows_html .= $row_layout->ReturnView();
    }

}

$output['posts'] = $rows_html;
echo json_encode($output);
exit;