<?php

require 'includes.php';

if(defined('PRIVATE_FORUM') AND PRIVATE_FORUM == true AND !Users_IsUserLoggedIn()){
	Leave(Users_SignInPageUrl());
}

if(isset($_GET['id'])){
    $id = intval($_GET['id']);
}else{
    Leave(FORUM_URL);
}

$admin = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "users WHERE id = $id ORDER BY id DESC LIMIT 0,1");

if(!$admin){
    $layout = GetPage('404', '{{ST:error_404}}');
    $layout->AddContentById('breadcrumbs', ' <li><a href="'.FORUM_URL.'">{{ST:home}}</a></li><li class="active">{{ST:error_404}}</li>');
    $layout->RenderViewAndExit();
}


$layout = GetPage('profiles', '{{ST:profile}}');

if(defined('SITE_NAME') AND SITE_NAME != ''){
	$layout->AddContentById('meta_title', SITE_NAME);
}
if(defined('SITE_META_DESCRIPTION') AND SITE_META_DESCRIPTION != ''){
	$layout->AddContentById('meta_desc', SITE_META_DESCRIPTION);
}

$layout->AddContentById('breadcrumbs', ' <li><a href="'.FORUM_URL.'">{{ST:home}}</a></li><li class="active">{{ST:profiles}}</li>');



if(isset($_GET['delete_photo']) AND intval($_GET['delete_photo']) == 1){
	if($admin->photo){
		$db->update(TABLES_PREFIX . "users", array('photo'=>''), array('id'=>$id), array("%s"));
		$file = $admin->photo;
		$exists = file_exists($file);
		if($exists){
			unlink($file);
			Leave('profiles.php?id='.$id.'&message=image_deleted');
		}else{
			$layout->AddContentById('alert', $layout->GetContent('alert'));
			$layout->AddContentById('alert_nature', ' alert-danger');
			$layout->AddContentById('alert_heading', '{{ST:error}}!');
			$layout->AddContentById('alert_message', '{{ST:image_does_not_exist}}');
		}
	}else{
		$layout->AddContentById('alert', $layout->GetContent('alert'));
		$layout->AddContentById('alert_nature', ' alert-danger');
		$layout->AddContentById('alert_heading', '{{ST:error}}!');
		$layout->AddContentById('alert_message', '{{ST:image_does_not_exist}}');
	}
}

if(isset($_GET['mute']) AND intval($_GET['mute']) == 1){
	if(Users_IsUserAdminOrModerator(Users_CurrentUserId())){
		$db->update(TABLES_PREFIX . "users", array('status'=>'muted'), array('id'=>$id), array("%s"));
		$layout->AddContentById('alert', $layout->GetContent('alert'));
		$layout->AddContentById('alert_nature', ' alert-success');
		$layout->AddContentById('alert_heading', '{{ST:success}}!');
		$layout->AddContentById('alert_message', '{{ST:the_user_has_been_muted}}');
		$admin = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "users WHERE id = $id ORDER BY id DESC LIMIT 0,1");
		
		$to = WEBMASTER_EMAIL;
		$subject = "[" . $_SERVER['SERVER_NAME'] . "]Forum: " . $strings->Get('muted_user');
		$message = $strings->Get('a_user_has_been_muted') . '

'.FORUM_URL.'user.php?id='.$id;
		$from = WEBMASTER_EMAIL;
		$headers = "From: $from";
		ini_set("sendmail_from", $from);	
		mail($to,$subject,$message,$headers);
	}
}

if(isset($_GET['unmute']) AND intval($_GET['unmute']) == 1){
	if(Users_IsUserAdminOrModerator(Users_CurrentUserId())){
		$db->update(TABLES_PREFIX . "users", array('status'=>'active'), array('id'=>$id), array("%s"));
		$layout->AddContentById('alert', $layout->GetContent('alert'));
		$layout->AddContentById('alert_nature', ' alert-success');
		$layout->AddContentById('alert_heading', '{{ST:success}}!');
		$layout->AddContentById('alert_message', '{{ST:the_user_has_been_unmuted}}');
		$admin = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "users WHERE id = $id ORDER BY id DESC LIMIT 0,1");
	}
}

if(isset($_GET['follow']) AND intval($_GET['follow']) == 1 AND Users_IsUserLoggedIn()){
    $db->insert(TABLES_PREFIX . "following", array('follower_id'=>Users_CurrentUserId(), 'user_id'=>$id), array("%d","%d"));
    Leave('profiles.php?id='.$id.'&message=followed');
}

if(isset($_GET['unfollow']) AND intval($_GET['unfollow']) == 1 AND Users_IsUserLoggedIn()){
    $db->query("DELETE FROM " . TABLES_PREFIX . "following WHERE user_id = $id AND follower_id = ".Users_CurrentUserId());
    Leave('profiles.php?id='.$id.'&message=unfollowed');
}

if(isset($_GET['message']) AND $_GET['message'] != ''){
	
	if($_GET['message'] == 'image_deleted'){
		$layout->AddContentById('alert', $layout->GetContent('alert'));
		$layout->AddContentById('alert_nature', ' alert-success');
		$layout->AddContentById('alert_heading', '{{ST:success}}!');
		$layout->AddContentById('alert_message', '{{ST:the_image_has_been_deleted}}');
	}

    if($_GET['message'] == 'followed'){
        $layout->AddContentById('alert', $layout->GetContent('alert'));
        $layout->AddContentById('alert_nature', ' alert-success');
        $layout->AddContentById('alert_heading', '{{ST:success}}!');
        $layout->AddContentById('alert_message', '{{ST:you_have_followed_this_user}}');
    }

    if($_GET['message'] == 'unfollowed'){
        $layout->AddContentById('alert', $layout->GetContent('alert'));
        $layout->AddContentById('alert_nature', ' alert-success');
        $layout->AddContentById('alert_heading', '{{ST:success}}!');
        $layout->AddContentById('alert_message', '{{ST:you_have_unfollowed_this_user}}');
    }
}

if((Users_CurrentUserId() == $id) OR !Users_IsUserLoggedIn()){
    $layout->AddContentById('hide_message_button', ' style="display: none;"');

    $layout->AddContentById('hide_follow_button', ' style="display: none;"');
    $layout->AddContentById('hide_unfollow_button', ' style="display: none;"');

    if(Users_CurrentUserId() != $id){
        $layout->AddContentById('hide_profile_button', ' style="display: none;"');
    }

}else{
    $following = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "following WHERE user_id = $id AND follower_id = ".Users_CurrentUserId()." ORDER BY id DESC LIMIT 0,1");
    if($following){
        $layout->AddContentById('hide_follow_button', ' style="display: none;"');
    }else{
        $layout->AddContentById('hide_unfollow_button', ' style="display: none;"');
    }

    if(Users_CurrentUserId() != $id){
        $layout->AddContentById('hide_profile_button', ' style="display: none;"');
    }
}

$layout->AddContentById('username', $admin->username);
if($admin->bio){
    $layout->AddContentById('bio', stripcslashes($admin->bio));
}
$layout->AddContentById('this_url', FORUM_URL.'profiles.php?id='.$id);
$layout->AddContentById('id', $id);
if($admin->photo){
	$layout->AddContentById('photo', $admin->photo);
	if(Users_IsUserAdminOrModerator(Users_CurrentUserId()) AND $admin->role == 'user'){
		$layout->AddContentById('delete_photo', '<p><a class="btn btn-danger btn-sm btn-block" onclick="return confirm(\'{{ST:are_you_sure}}\');" title="{{ST:delete}}" href="'.FORUM_URL.'profiles.php?id='.$id.'&delete_photo=1"><span class="glyphicon glyphicon-trash"></span> {{ST:delete_photo}}</a></p>');
	}
}else{
	$layout->AddContentById('photo', FORUM_URL.'img/anon.png');
}


if(Users_CurrentUserId() != $id){
    if($admin->status != 'banned'){
        if(Users_IsUserAdmin(Users_CurrentUserId()) AND $admin->status != 'pending'){
            if($admin->status == 'active'){
                $layout->AddContentById('unmute_state', 'style="display:none"');
            }elseif($admin->status == 'muted'){
                $layout->AddContentById('mute_state', 'style="display:none"');
            }
        }elseif(Users_IsUserModerator(Users_CurrentUserId()) AND $admin->role == 'user'){
            if($admin->status == 'active'){
                $layout->AddContentById('unmute_state', 'style="display:none"');
            }elseif($admin->status == 'muted'){
                $layout->AddContentById('mute_state', 'style="display:none"');
            }
        }else{
            $layout->AddContentById('mute_unmute_state', ' display:none;');
        }
    }else{
        $layout->AddContentById('mute_unmute_state', ' display:none;');
    }
}else{
    $layout->AddContentById('mute_unmute_state', ' display:none;');
}

if($admin->facebook_url){
	$layout->AddContentById('facebook_url', '<a class="facebook-icon" href="'.$admin->facebook_url.'" target="_blank"><i class="simple-forum-icon-facebook-rect"></i></a>&nbsp;');
}

if($admin->twitter_url){
	$layout->AddContentById('twitter_url', '<a class="twitter-icon" href="'.$admin->twitter_url.'" target="_blank"><i class="simple-forum-icon-twitter-bird-1"></i></a>&nbsp;');
}

if($admin->google_url){
    $layout->AddContentById('google_url', '<a class="google-icon" href="'.$admin->google_url.'" target="_blank"><i class="simple-forum-icon-googleplus-rect"></i></a>&nbsp;');
}

if($admin->website_url){
	$layout->AddContentById('website_url', '<a href="'.$admin->website_url.'" target="_blank"><span class="glyphicon glyphicon-globe"></span></a>&nbsp;');
}

$badges = Users_GetUserBadges($id);
if($badges){
    $layout->AddContentById('user_badges', '<strong>{{ST:badges}}: </strong> ' . $badges);
}

if(isset($_GET['like']) AND intval($_GET['like']) == 1 AND isset($_GET['post'])){
    if(Users_CurrentUserId()){
        if(count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "likes WHERE user_id = ".intval(Users_CurrentUserId())." AND post_id = ".intval($_GET['post'])."" )) > 0){

        }else{
            $db->insert(TABLES_PREFIX . "likes", array('post_id'=>intval($_GET['post']),'user_id'=>intval(Users_CurrentUserId())), array("%d","%d"));

            $the_post = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = " .intval($_GET['post']) . " ORDER BY id DESC LIMIT 0,1");
            $db->update(TABLES_PREFIX . "posts", array('likes'=>(1+$the_post->likes)), array('id'=>intval($_GET['post'])), array("%d"));

            $layout->AddContentById('alert', $layout->GetContent('alert'));
            $layout->AddContentById('alert_nature', ' alert-success');
            $layout->AddContentById('alert_heading', '{{ST:success}}!');
            $layout->AddContentById('alert_message', '{{ST:you_have_liked_the_post}}');
        }
    }
}

if(isset($_GET['unlike']) AND intval($_GET['unlike']) == 1 AND isset($_GET['post'])){
    if(Users_CurrentUserId()){
        if(count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "likes WHERE user_id = ".intval(Users_CurrentUserId())." AND post_id = ".intval($_GET['post'])."" )) > 0){
            $db->query("DELETE FROM " . TABLES_PREFIX . "likes WHERE post_id = " . intval($_GET['post']) . " AND user_id = " . intval(Users_CurrentUserId()));

            $the_post = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = " .intval($_GET['post']) . " ORDER BY id DESC LIMIT 0,1");
            $db->update(TABLES_PREFIX . "posts", array('likes'=>($the_post->likes - 1)), array('id'=>intval($_GET['post'])), array("%d"));

            $layout->AddContentById('alert', $layout->GetContent('alert'));
            $layout->AddContentById('alert_nature', ' alert-success');
            $layout->AddContentById('alert_heading', '{{ST:success}}!');
            $layout->AddContentById('alert_message', '{{ST:you_have_unliked_the_post}}');
        }
    }
}

if(isset($_GET['delete']) AND intval($_GET['delete']) == 1 AND isset($_GET['post'])){
    $the_post = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = " .intval($_GET['post']) . " ORDER BY id DESC LIMIT 0,1");

    if(Users_IsUserAdminOrModerator(Users_CurrentUserId()) OR Users_CurrentUserId() == $the_post->user_id){

        if($the_post->photos != ''){
            $files = unserialize($the_post->photos);
            if(count($files) > 0 AND is_array($files)){
                foreach($files as $f){
                    $file = 'uploads/' . $f;
                    $exists = is_file($file);
                    if($exists){
                        unlink($file);
                    }
                }
            }
        }

        if($the_post->is_question == 'y'){
            $all_children = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = ".intval($_GET['post'])." OR parent_id = ".intval($_GET['post'])."" );
            if($all_children){
                foreach($all_children as $child){
                    $db->query("DELETE FROM " . TABLES_PREFIX . "posts_following WHERE post_id = " . intval($child->id) );
                    $db->query("DELETE FROM " . TABLES_PREFIX . "likes WHERE post_id = " . intval($child->id));
                    $db->query("DELETE FROM " . TABLES_PREFIX . "flags WHERE post_id = " . intval($child->id));
                    $db->query("DELETE FROM " . TABLES_PREFIX . "posts WHERE id = " . intval($child->id));
                }
            }
            $layout->AddContentById('alert', $layout->GetContent('alert'));
            $layout->AddContentById('alert_nature', ' alert-success');
            $layout->AddContentById('alert_heading', '{{ST:success}}!');
            $layout->AddContentById('alert_message', '{{ST:the_post_has_been_deleted}}');
        }else{
            $db->query("DELETE FROM " . TABLES_PREFIX . "posts_following WHERE post_id = " . intval($_GET['post']) );
            $db->query("DELETE FROM " . TABLES_PREFIX . "likes WHERE post_id = " . intval($_GET['post']));
            $db->query("DELETE FROM " . TABLES_PREFIX . "flags WHERE post_id = " . intval($_GET['post']));
            $db->query("DELETE FROM " . TABLES_PREFIX . "posts WHERE id = " . intval($_GET['post']));

            $layout->AddContentById('alert', $layout->GetContent('alert'));
            $layout->AddContentById('alert_nature', ' alert-success');
            $layout->AddContentById('alert_heading', '{{ST:success}}!');
            $layout->AddContentById('alert_message', '{{ST:the_post_has_been_deleted}}');
        }
    }
}

if(isset($_GET['flag']) AND intval($_GET['flag']) == 1 AND isset($_GET['post'])){
    $flag_response = flag_a_post(intval($_GET['post']));

    if($flag_response['type'] == 'success'){
        $layout->AddContentById('alert', $layout->GetContent('alert'));
        $layout->AddContentById('alert_nature', ' alert-success');
        $layout->AddContentById('alert_heading', '{{ST:success}}!');
        $layout->AddContentById('alert_message', $flag_response['msg']);
    }else{
        $layout->AddContentById('alert', $layout->GetContent('alert'));
        $layout->AddContentById('alert_nature', ' alert-danger');
        $layout->AddContentById('alert_heading', '{{ST:error}}!');
        $layout->AddContentById('alert_message', $flag_response['msg']);
    }
}



if(isset($_GET['followers'])){
    $layout->AddContentById('followers_active', ' active');

    $rows = 30;
    $number_of_records = count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "following WHERE user_id =".intval($id)."" ));
    $number_of_pages = ceil( $number_of_records / $rows );
    if(isset($_GET['page'])){
        $page = intval($_GET['page']);
    }else{
        $page = 1;
    }

    $offset = ($page - 1) * $rows;
    $layout->AddContentById('page', $page);

    $latest = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "following WHERE user_id =".intval($id)." LIMIT $offset, $rows");


    $rows_html = '';
    if($latest){
        foreach($latest as $post){
            $row_layout = new Layout('html/','str/');
            $row_layout->SetContentView('profiles-users-rows');
            $row_layout->AddContentById('id', $post->follower_id);

            $row_layout->AddContentById('page', $page);

            $row_layout->AddContentById('followers_following', 'following');
            $row_layout->AddContentById('profile_id', $id);

            $user_details = Users_GetUserDetails($post->follower_id);
            if($user_details){
                if($user_details['is_admin'] == true){
                    $row_layout->AddContentById('is_admin', '<img title="{{ST:is_admin}}" data-toggle="tooltip" src="{{ID:base_url}}img/admin.png">');
                }elseif($user_details['is_moderator'] == true){
                    $row_layout->AddContentById('is_admin', '<img title="{{ST:moderator}}" data-toggle="tooltip" src="{{ID:base_url}}img/moderator.png">');
                }
                if($user_details['username']){
                    $row_layout->AddContentById('user_name', $user_details['username']);
                }
                if($user_details['path_to_profile']){
                    $row_layout->AddContentById('path_to_profile', $user_details['path_to_profile']);
                }
                if($user_details['path_to_photo']){
                    $row_layout->AddContentById('user_photo', $user_details['path_to_photo']);
                }else{
                    $row_layout->AddContentById('user_photo', FORUM_URL.'img/anon.png');
                }
            }

            //$row_layout->AddContentById('user_badges', Users_GetUserBadges($post->user_id));

            if((Users_CurrentUserId() == $post->follower_id) OR !Users_IsUserLoggedIn()){
                $row_layout->AddContentById('hide_message_button', ' style="display: none;"');

                $row_layout->AddContentById('hide_follow_button', ' style="display: none;"');
                $row_layout->AddContentById('hide_unfollow_button', ' style="display: none;"');

                if(Users_CurrentUserId() != $post->follower_id){
                    $row_layout->AddContentById('hide_profile_button', ' style="display: none;"');
                }

            }else{
                $following = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "following WHERE user_id = ".$post->follower_id." AND follower_id = ".Users_CurrentUserId()." ORDER BY id DESC LIMIT 0,1");
                if($following){
                    $row_layout->AddContentById('hide_follow_button', ' style="display: none;"');
                }else{
                    $row_layout->AddContentById('hide_unfollow_button', ' style="display: none;"');
                }

                if(Users_CurrentUserId() != $post->follower_id){
                    $row_layout->AddContentById('hide_profile_button', ' style="display: none;"');
                }
            }

            $rows_html .= $row_layout-> ReturnView();
        }

        if($number_of_records>$rows){
            $pagination = Paginate(FORUM_URL.'profiles.php?following=1&id='.intval($id), $page, $number_of_pages, true, 3);
            $layout->AddContentById('pagination', $pagination);
        }

    }else{
        $rows_html = '<p>{{ST:there_are_no_items}}</p>';
    }



    $layout->AddContentById('rows', $rows_html);
}elseif(isset($_GET['following'])){
    $layout->AddContentById('following_active', ' active');

    $rows = 30;
    $number_of_records = count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "following WHERE follower_id =".intval($id)."" ));
    $number_of_pages = ceil( $number_of_records / $rows );
    if(isset($_GET['page'])){
        $page = intval($_GET['page']);
    }else{
        $page = 1;
    }

    $offset = ($page - 1) * $rows;
    $layout->AddContentById('page', $page);

    $latest = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "following WHERE follower_id =".intval($id)." LIMIT $offset, $rows");


    $rows_html = '';
    if($latest){
        foreach($latest as $post){
                    $row_layout = new Layout('html/','str/');
                    $row_layout->SetContentView('profiles-users-rows');
                    $row_layout->AddContentById('id', $post->user_id);

                    $row_layout->AddContentById('page', $page);

                    $row_layout->AddContentById('followers_following', 'following');
                    $row_layout->AddContentById('profile_id', $id);

                    $user_details = Users_GetUserDetails($post->user_id);
                    if($user_details){
                        if($user_details['is_admin'] == true){
                            $row_layout->AddContentById('is_admin', '<img title="{{ST:is_admin}}" data-toggle="tooltip" src="{{ID:base_url}}img/admin.png">');
                        }elseif($user_details['is_moderator'] == true){
                            $row_layout->AddContentById('is_admin', '<img title="{{ST:moderator}}" data-toggle="tooltip" src="{{ID:base_url}}img/moderator.png">');
                        }
                        if($user_details['username']){
                            $row_layout->AddContentById('user_name', $user_details['username']);
                        }
                        if($user_details['path_to_profile']){
                            $row_layout->AddContentById('path_to_profile', $user_details['path_to_profile']);
                        }
                        if($user_details['path_to_photo']){
                            $row_layout->AddContentById('user_photo', $user_details['path_to_photo']);
                        }else{
                            $row_layout->AddContentById('user_photo', FORUM_URL.'img/anon.png');
                        }
                    }

                    //$row_layout->AddContentById('user_badges', Users_GetUserBadges($post->user_id));

            if((Users_CurrentUserId() == $post->user_id) OR !Users_IsUserLoggedIn()){
                $row_layout->AddContentById('hide_message_button', ' style="display: none;"');

                $row_layout->AddContentById('hide_follow_button', ' style="display: none;"');
                $row_layout->AddContentById('hide_unfollow_button', ' style="display: none;"');

                if(Users_CurrentUserId() != $post->user_id){
                    $row_layout->AddContentById('hide_profile_button', ' style="display: none;"');
                }

            }else{
                $following = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "following WHERE user_id = ".$post->user_id." AND follower_id = ".Users_CurrentUserId()." ORDER BY id DESC LIMIT 0,1");
                if($following){
                    $row_layout->AddContentById('hide_follow_button', ' style="display: none;"');
                }else{
                    $row_layout->AddContentById('hide_unfollow_button', ' style="display: none;"');
                }

                if(Users_CurrentUserId() != $post->user_id){
                    $row_layout->AddContentById('hide_profile_button', ' style="display: none;"');
                }
            }

                $rows_html .= $row_layout-> ReturnView();
        }

        if($number_of_records>$rows){
            $pagination = Paginate(FORUM_URL.'profiles.php?following=1&id='.intval($id), $page, $number_of_pages, true, 3);
            $layout->AddContentById('pagination', $pagination);
        }

    }else{
        $rows_html = '<p>{{ST:there_are_no_items}}</p>';
    }



    $layout->AddContentById('rows', $rows_html);

}else{
    $layout->AddContentById('posts_active', ' active');

    $rows = ROWS_PER_PAGE;
    $number_of_records = count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts WHERE approved = 'y' AND flags < ".HIDE_POST_ON_THIS_NUMBER_OF_FLAGS." AND user_id = ".intval($id)."" ));
    $number_of_pages = ceil( $number_of_records / $rows );
    if(isset($_GET['page'])){
        $page = intval($_GET['page']);
    }else{
        $page = 1;
    }

    $offset = ($page - 1) * $rows;
    $layout->AddContentById('page', $page);

    $latest = $db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts WHERE approved = 'y' AND flags < ".HIDE_POST_ON_THIS_NUMBER_OF_FLAGS." AND user_id = ".intval($id)." ORDER BY date DESC LIMIT $offset, $rows");



    $rows_html = '';
    if($latest){
        foreach($latest as $post){
            if($post->approved == 'y'){

                    $row_layout = new Layout('html/','str/');
                    $row_layout->SetContentView('profiles-posts-row');
                    $row_layout->AddContentById('id', $post->id);

                    $row_layout->AddContentById('page', $page);

                    if($post->is_question == 'y'){
                        $question = $post;
                    }else{
                        $question = $db->get_row("SELECT * FROM " . TABLES_PREFIX . "posts WHERE id = ".$post->parent_id." ORDER BY id DESC LIMIT 0,1");
                    }

                    $row_layout->AddContentById('thread_id', $question->id);
                    $row_layout->AddContentById('profile_id', $id);

                    if($post->photos != ''){
                        $files = unserialize($post->photos);
                        if(count($files) > 0 AND is_array($files)){
                            $files_lists = '<br/><p>';
                            $files_count = 1;
                            foreach($files as $f){
                                $files_lists .= '<img src="'.get_file_icon_path($f).'">&nbsp;<a target="_blank" href="'. FORUM_URL . 'uploads/' . $f.'">{{ST:attachment}} '.$files_count.'</a>&nbsp;<br/>';
                                $files_count++;
                            }
                            $files_lists .= '</p>';
                            $row_layout->AddContentById('files', $files_lists);
                        }
                    }


                    $signature = "";

                    $row_layout->AddContentById('likes', NiceNumber($post->likes));

                    $row_layout->AddContentById('date', getRelativeTime($post->date));

                    $row_layout->AddContentById('user_id', $post->user_id);

                    $user_details = Users_GetUserDetails($post->user_id);
                    if($user_details){
                        if($user_details['is_admin'] == true){
                            $row_layout->AddContentById('is_admin', '<img title="{{ST:is_admin}}" data-toggle="tooltip" src="{{ID:base_url}}img/admin.png">');
                        }elseif($user_details['is_moderator'] == true){
                            $row_layout->AddContentById('is_admin', '<img title="{{ST:moderator}}" data-toggle="tooltip" src="{{ID:base_url}}img/moderator.png">');
                        }
                        if($user_details['username']){
                            $row_layout->AddContentById('user_name', $user_details['username']);
                        }
                        if($user_details['path_to_profile']){
                            $row_layout->AddContentById('path_to_profile', $user_details['path_to_profile']);
                        }
                        if($user_details['path_to_photo']){
                            $row_layout->AddContentById('user_photo', $user_details['path_to_photo']);
                        }else{
                            $row_layout->AddContentById('user_photo', FORUM_URL.'img/anon.png');
                        }

                        if($user_details['signature']){
                            $signature = "<hr/><div class='muted'>" . $user_details['signature'] . "</div>";

                            $row_layout->AddContentById('signature', $signature);
                        }
                    }

                if(defined('SEO_HUMAN_FRIENDLY_URLS') AND SEO_HUMAN_FRIENDLY_URLS == true){
                    $thread_url = '{{ID:base_url}}thread/'.UrlText($question->title).'/'.$question->id.'/';
                }else{
                    $thread_url = '{{ID:base_url}}thread.php?id='.$question->id;
                }

                    if($post->quote AND $post->quote != ''){
                        $row_layout->AddContentById('question', '<h4>{{ST:thread_title}}: <a href="'.$thread_url.'">'.$question->title.'</a></h4>' . $post->quote . $post->body);
                    }else{
                        $row_layout->AddContentById('question', '<h4>{{ST:thread_title}}: <a href="'.$thread_url.'">'.$question->title.'</a></h4>' . $post->body);
                    }


                    if(!$badges){
                        $row_layout->AddContentById('user_badges', '');
                        $row_layout->AddContentById('display_badges', 'style="display: none;"');
                    }else{
                        $row_layout->AddContentById('user_badges', $badges);
                    }

                    if(!Users_IsUserAdminOrModerator(Users_CurrentUserId())){
                        if(Users_CurrentUserId() != intval($post->user_id)){
                            $row_layout->AddContentById('edit_state', 'style="display:none"');
                            $row_layout->AddContentById('delete_state', 'style="display:none"');
                        }
                    }

                    if(!Users_IsUserLoggedIn()){
                        $row_layout->AddContentById('like_or_un_get', 'like');
                        $row_layout->AddContentById('like_or_un', 'btn-default');
                        $row_layout->AddContentById('like_or_un_text', '{{ST:like}}');
                        $row_layout->AddContentById('like_alert', 'onclick="return SignInAlert();"');
                        $row_layout->AddContentById('flag_alert', 'onclick="return SignInAlert();"');
                    }elseif(count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "likes WHERE user_id = ".intval(Users_CurrentUserId())." AND post_id = ".intval($post->id)."" )) > 0){

                        $row_layout->AddContentById('like_or_un_get', 'unlike');
                        $row_layout->AddContentById('like_or_un', 'btn-success');
                        $row_layout->AddContentById('like_or_un_text', '{{ST:unlike}}');
                        $row_layout->AddContentById('flag_alert', 'onclick="return confirm(\'{{ST:are_you_sure}}\');"');
                    }else{
                        $row_layout->AddContentById('like_or_un_get', 'like');
                        $row_layout->AddContentById('like_or_un', 'btn-default');
                        $row_layout->AddContentById('like_or_un_text', '{{ST:like}}');
                        $row_layout->AddContentById('flag_alert', 'onclick="return confirm(\'{{ST:are_you_sure}}\');"');
                    }

                    if($question->locked == 'y'){
                        $row_layout->AddContentById('post_alert', 'onclick="return LockedAlert();"');
                    }

                //}

                $rows_html .= $row_layout-> ReturnView();
            }
        }

        if($number_of_records>$rows){
            $pagination = Paginate(FORUM_URL.'profiles.php?id='.intval($id), $page, $number_of_pages, true, 3);
            $layout->AddContentById('pagination', $pagination);
        }

    }else{
        $rows_html = '<p>{{ST:there_are_no_posts}}</p>';
    }



    $layout->AddContentById('rows', '<div class="col-md-12">' . $rows_html . '</div>');
}

$layout->AddContentById('total_posts', count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "posts WHERE approved = 'y' AND flags < ".HIDE_POST_ON_THIS_NUMBER_OF_FLAGS." AND user_id =".intval($id) )));
$layout->AddContentById('total_followers', count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "following WHERE user_id =".intval($id) )));
$layout->AddContentById('total_following', count($db->get_results("SELECT * FROM " . TABLES_PREFIX . "following WHERE follower_id =".intval($id) )));




$layout->RenderViewAndExit();
