<?php

require_once 'php/config.php';
require_once 'php/db.php';
require_once 'php/delta.php';

$sql = "CREATE TABLE " . TABLES_PREFIX . "options (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
option_key text,
option_value text,
PRIMARY KEY  (id)
);";
delta($sql);

$sql = "CREATE TABLE " . TABLES_PREFIX . "categories (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
name text,
order_by int(11) NOT NULL DEFAULT '9999',
locked varchar(1) NOT NULL DEFAULT 'n',
parent bigint(20) unsigned NOT NULL DEFAULT '0',
description text,
PRIMARY KEY  (id)
);";
delta($sql);

$sql = "CREATE TABLE " . TABLES_PREFIX . "flags (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
user_id int(11) NOT NULL,
post_id int(11) NOT NULL,
reason text,
date timestamp NULL DEFAULT NULL,
PRIMARY KEY  (id)
);";
delta($sql);

$sql = "CREATE TABLE " . TABLES_PREFIX . "likes (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
post_id int(11) NOT NULL,
user_id int(11) NOT NULL,
PRIMARY KEY  (id)
);";
delta($sql);

$sql = "CREATE TABLE " . TABLES_PREFIX . "posts (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
title text,
body text NOT NULL,
user_id int(11) NOT NULL,
category_id int(11) NOT NULL,
parent_id int(11) DEFAULT NULL,
likes int(11) DEFAULT NULL,
is_question varchar(1) NOT NULL,
flags int(11) NOT NULL DEFAULT '0',
date timestamp NULL DEFAULT NULL,
views bigint(20) NOT NULL DEFAULT '0',
last_edited_on timestamp NULL DEFAULT NULL,
last_edited_by int(11) DEFAULT NULL,
locked varchar(1) NOT NULL DEFAULT 'n',
approved varchar(1) NOT NULL DEFAULT 'y',
pinned int(11) NOT NULL DEFAULT '0',
quote text,
photos text,
PRIMARY KEY  (id)
);";
delta($sql);

$sql = "CREATE TABLE " . TABLES_PREFIX . "posts_following (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
user_id int(11) NOT NULL,
post_id int(11) NOT NULL,
PRIMARY KEY  (id)
);";
delta($sql);

$sql = "CREATE TABLE " . TABLES_PREFIX . "users (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
username text NOT NULL,
email text NOT NULL,
password text NOT NULL,
status varchar(10) NOT NULL,
likes int(11) DEFAULT NULL,
role varchar(10) NOT NULL,
notes text,
hash text,
photo text,
bio text,
facebook_id text,
added_on TIMESTAMP NULL,
achievements text,
last_seen TIMESTAMP NULL,
website_url text,
facebook_url text,
twitter_url text,
google_url text,
signature text,
PRIMARY KEY  (id)
);";
delta($sql);

$sql = "CREATE TABLE " . TABLES_PREFIX . "achievements (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
name text NOT NULL,
type text NOT NULL,
start_from int(11) DEFAULT NULL,
end_at int(11) DEFAULT NULL,
icon text,
description text,
PRIMARY KEY  (id)
);";
delta($sql);

$sql = "CREATE TABLE " . TABLES_PREFIX . "messages (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
message text NOT NULL,
owner_id bigint(20) NOT NULL,
contact_id bigint(20) NOT NULL,
sender bigint(20) NOT NULL,
seen varchar(1) NOT NULL DEFAULT 'n',
date_sent timestamp NULL DEFAULT NULL,
PRIMARY KEY  (id)
);";
delta($sql);

$sql = "CREATE TABLE " . TABLES_PREFIX . "contacts (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
user_id bigint(20) NOT NULL,
contact_id bigint(20) NOT NULL,
contact_username text NOT NULL,
PRIMARY KEY  (id)
);";
delta($sql);

$sql = "CREATE TABLE " . TABLES_PREFIX . "following (
id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
user_id bigint(20) NOT NULL,
follower_id bigint(20) NOT NULL,
PRIMARY KEY  (id)
);";
delta($sql);
