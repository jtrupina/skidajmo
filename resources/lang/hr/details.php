<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'autor' => 'Autor',
    'dodano'     => 'Dodano',
    'verzija' => 'Verzija',
    'velicina' => 'Veličina',
    'preuzimanje' => 'Preuzimanje',
    'licenca' => 'Licenca',
    'platforma' => 'Platforma',
    'kategorija' => 'Kategorija',
    'podijeli' => 'PODIJELI',
    'naseocjene' => 'NAŠE OCJENE',
    'sucelje' => 'SUČELJE',
    'mogucnosti' => 'MOGUĆNOSTI',
    'jednostavnost' => 'JEDNOSTAVNOST',
    'nasaocjena' => 'NAŠA OCJENA',
    'najnoviji' => 'Najnoviji programi iz ove kategorije',
    'vasaocjena' => 'VAŠA OCJENA',
    'nasapreporuka' => 'Naša preporuka:',
    'glasova' => 'glasova',

];
