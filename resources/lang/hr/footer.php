<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'korisnilinkovi' => 'Korisni linkovi',
    'oglas'     => 'Vaš oglas ovdje',
    'statistika' => 'Statistika',
    'programa' => 'Programa',
    'kategorija' => 'Kategorija',
    'ukupnavelicina' => 'Ukupna veličina',
    'korisnikaonline' => 'Korisnika online',
    'search' => 'upišite ključnu riječ, npr. Avast'

];

