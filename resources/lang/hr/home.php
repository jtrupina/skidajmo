<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'naslovnica' => 'NASLOVNICA',
    'onama'     => 'O nama',
    'kategorije' => 'Kategorije',
    'pitanja' => 'Pitanja',
    'preporuka' => 'Naša preporuka',
    'popularnisoftver' => 'Popularni softver',
    'dodano' => 'Dodano',
    'verzija' => 'Verzija',
    'velicina' => 'Veličina',
    'osvjezeno' => 'OSVJEŽENO',
    'week' => 'PROGRAM TJEDNA',
    'pretrazivanje' => 'Rezultati pretraživanja',



];