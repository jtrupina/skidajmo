<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'naslovnica' => 'Naslovnica',
    'onama'     => 'O nama',
    'sadadownload' => 'Što se sad skida',
    'sigurnost' => 'Sigurnost',
    'dodaj' => 'Dodaj softver',
    'mojalista' => 'Moja lista',
    'brprograma' => 'broj programa',
    'search' => 'upišite ključnu riječ, npr. Avast',
    'login' => 'Trenutno pregledavate sadržaj kao gost. Prijavite se putem Facebooka',
    'logout' => 'Odjava',
    'pitanja' => 'Pitanja',
    'bok' => 'Bok',

];
