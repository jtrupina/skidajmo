<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'dodaj' => 'Dodajte Vaš softver na skidajmo.com',
    'doyouknow'     => 'Znate za aplikaciju koja se još ne nalazi u našem repozitoriju softvera?
Ukoliko ste razvili neku zanimljivu aplikaciju, dodajte istu na skidajmo.com.',
    'softwarename' => 'Naziv programa',
    'mail' => 'Vaš email',
    'surl' => 'Web adresa programa',
    'sdesc' => 'Kratak opis programa',
    'spam' => 'Spam provjera: Koliko je',
    'dodaj2' => 'DODAJ PROGRAM',
    'publish' => 'OBJAVIT ĆEMO SAMO BESPLATNE APLIKACIJE. UKOLIKO JE VAŠA APLIKACIJA SHAREWARE ILI TRIAL VERZIJA MOLIMO POGLEDAJTE DONJE UVJETE PROMIDŽBE NA SKIDAJMO.COM',
    'commercial' => 'Vaša komercijalna aplikacija na Skidajmo.com?',
    'price' => '500 kn / jednokratno',
    'visits' => 'Vašu aplikaciju mjesečno vidi preko 600 000 korisnika
Vaša aplikacija smještena je na našim serverima
Povećajte prodaju svojih aplikacija na najposjećenijem download portalu na Balkanu',
    'sendsoftware' => 'Pošaljite nam vašu aplikaciju'

];