<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'naslovnica' => 'HOMEPAGE',
    'onama'     => 'About us',
    'kategorije' => 'Categories',
    'pitanja' => 'Questions',
    'preporuka' => 'We recommend',
    'popularnisoftver' => 'Popular Software',
    'dodano' => 'Added',
    'verzija' => 'Version',
    'velicina' => 'Size',
    'osvjezeno' => 'UPDATED',
    'week' => 'PROGRAM OF WEEK',
    'pretrazivanje' => 'Search results',

];
