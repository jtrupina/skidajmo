<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'dodaj' => 'Add your software to skidajmo.com',
    'doyouknow'     => 'Do you know of an application that is not yet in our repository software?
If you have developed an interesting application, add the same to skidajmo.com.',
    'softwarename' => 'Software name',
    'mail' => 'Your email',
    'surl' => 'Software URL',
    'sdesc' => 'Software description',
    'spam' => 'Spam Detection: How much is',
    'dodaj2' => 'Add software',
    'publish' => 'We will publish only free software.
If Your Software is shareware or trial version please see the CONDITIONS OF CAMPAIGN ON SKIDAJMO.COM',
    'commercial' => 'Your commercial software on Skidajmo.com?',
    'price' => '70$ / one-time',
    'visits' => 'Your software each month sees over 600,000 users
Your application is located on our servers
Increase sales of Your Softvare on the most visited portal on Balkan',
    'sendsoftware' => 'Send us your application'

];