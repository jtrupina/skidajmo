<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'naslovnica' => 'Home',
    'onama'     => 'About us',
    'sadadownload' => 'Now downloading',
    'sigurnost' => 'Security',
    'dodaj' => 'Add Software',
    'mojalista' => 'My list',
    'brprograma' => 'programs',
    'search' => 'type key word, eg. Avast',
    'login' => 'Currently you are viewing content as guest. Sign in via Facebook',
    'logout' => 'Logout',
    'pitanja' => 'Questions',
    'bok' => 'Hi',

];
