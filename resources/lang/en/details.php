<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'autor' => 'Author',
    'dodano'     => 'Added',
    'verzija' => 'Version',
    'velicina' => 'Size',
    'preuzimanje' => 'Downloads',
    'licenca' => 'License',
    'platforma' => 'Platform',
    'kategorija' => 'Category',
    'podijeli' => 'SHARE',
    'naseocjene' => 'OUR RATINGS',
    'sucelje' => 'INTERFACE',
    'mogucnosti' => 'FUTURES',
    'jednostavnost' => 'SIMPLICITY',
    'nasaocjena' => 'OUR SCORE',
    'najnoviji' => 'Latest programs from category',
    'vasaocjena' => 'YOUR SCORE',
    'nasapreporuka' => 'Our recommendation',
    'glasova' => 'votes'

];
