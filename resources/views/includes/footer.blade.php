<footer class="mainFooter-box">
    <div class="footerList">
        <div class="col1">
            <h1 class="title">{{ trans('footer.korisnilinkovi') }}</h1>
            <ol class="linkList">
                <li class="linkItem"><a href="">Filesfreeware.com</a></li>
                <li class="linkItem"><a href="">Besplatnestvari.biz</a></li>
                <li class="linkItem"><a href="">HD Televizija</a></li>
                <li class="linkItem"><a href="">Racunalo.com</a></li>
                <li class="linkItem"><a href="">CroSatelite.com</a></li>
                <li class="linkItem"><a href="">linuxzasve</a></li>
            </ol>
        </div>
        <div class="col2">
            <h1 class="title">{{ trans('footer.oglas') }}</h1>
            <div class="image"></div>
        </div>
        <div class="col3">
            <h1 class="title">Facebook</h1>

        </div>
        <div class="col4">
            <h1 class="title">{{ trans('footer.statistika') }}</h1>
            <ol class="statisticList">
                <li class="statisticItem">{{ trans('footer.programa') }}: <span class="number">{!! \App\Http\Controllers\MenuController::getTotalNumberOfSoftware() !!}</span></li>
                <li class="statisticItem">{{ trans('footer.kategorija') }}: <span class="number">{!! \App\Http\Controllers\MenuController::getTotalNumberOfCategories() !!}</span></li>
                <li class="statisticItem">{{ trans('footer.ukupnavelicina') }}: <span class="number">{!! \App\Http\Controllers\MenuController::getTotalSizeOfSoftware() !!}</span></li>
                <li class="statisticItem">{{ trans('footer.korisnikaonline') }}: <span class="number">{{ rand(200, 250) }}</span></li>
            </ol>
            <div class="image"></div>
        </div>
    </div>
    <div class="copyright-box">
        <div class="text-box">
            <p class="text1">Copyright @ by Ivica Kutnjak | Skidajmo.com 2007.-{{ date('Y') }}. | RSS Feed | Skidajmo.com koristi i preporučuje AVALON hosting | Web by idip.hr</p>
            <p class="text2">Najbolji besplatni programi i igrice na jednom mjestu | Free software in one place. | Free Software and Games.</p>
        </div>
        <div class="socialIcon-box">
            <div class="socialIcon-List">
                <div class="socialIcon-Element">
                    <div class="socialIcon-Icon1"><span class="icon"></span></div>
                </div>
                <div class="socialIcon-Element">
                    <div class="socialIcon-Icon2"><span class="icon"></span></div>
                </div>
                <div class="socialIcon-Element">
                    <div class="socialIcon-Icon3"><span class="icon"></span></div>
                </div>
                <div class="socialIcon-Element">
                    <div class="socialIcon-Icon4"><span class="icon"></span></div>
                </div>
                <div class="socialIcon-Element">
                    <div class="socialIcon-Icon5"><span class="icon"></span></div>
                </div>
            </div>
        </div>
    </div>
</footer>