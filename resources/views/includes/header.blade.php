<div class="topHeader">
    <div class="box">
        @if(Auth::check())
        <a class="image icon5" style="color: white">Bok {{ Auth::User()->name }}!</a>
        @endif
        <div class="signIn">
            @if(!Auth::check())
                {{ trans('navigation.login') }}<a class="text" href="{{ route('social.login', ['facebook']) }}"><img src="{{ asset('slike/facebook-prijava.png') }}"></a>
            @else
                <a class="text" href="{{ url('logout') }}">{{ trans('navigation.logout') }}</a>
            @endif
        </div>
    </div>
</div>
<div class="topFooterSmall">
    <div class="icon1 menu1">

    </div>
    <div class="leftLogo-boxSmall">
        <img class="logo" src="{{ asset('user/img/logo-gore-top.png') }}" alt="">
    </div>
    <div class="icon1 menu2">

    </div>
    <hr class="line">
    <form class="mainSearchSmall" method="post" action="">
        <input class="input" placeholder="upišite ključnu riječ, npr. Avast">
        <button class="button"><span class="icon"></span></button>
    </form>
</div>
<header class="mainHeader-box">
    <nav class="topNav">
        <ul class="nav-list">
            <li class="nav-element -first {{ Request::is('/') ? 'active' : '' }}">
                <a class="nav-text" href="{{ url('/') }}">{{ trans('navigation.naslovnica') }}</a>
            </li>
            <li class="nav-element {{ Request::is('o-nama') ? 'active' : '' }}">
                <a class="nav-text" href="{{ url('/o-nama') }}">{{ trans('navigation.onama') }}</a>
            </li>
            <li class="nav-element {{ Request::is('top-50') ? 'active' : '' }}">
                <a class="nav-text" href="{{ url('/top-50') }}">TOP 50</a>
            </li>
            <li class="nav-element {{ Request::is('str/sto-se-skida') ? 'active' : '' }}">
                <a class="nav-text" href="{{ url('str/sto-se-skida') }}">{{ trans('navigation.sadadownload') }}</a>
            </li>
            <li class="nav-element">
                <a class="nav-text">{{ trans('navigation.sigurnost') }}</a>
            </li>
            <li class="nav-element  {{ Request::is('dodaj') ? 'active' : '' }}">
                <a class="nav-text" href="{{ url('/dodaj') }}">{{ trans('navigation.dodaj') }}</a>
            </li>
            <li class="nav-element-photo">

                <form method="post" action="{{ route('changelocale') }}" id="lform">
                    <div class="dropdown">
                        @if(Session::has('applocale'))
                            @if(Session::get('applocale') == 'hr')
                                <input hidden name="applocale" value="en">
                                <button class="dropbtn" value="hr" selected>
                                    <img class="img" src="{{ asset('user/img/croatian.png') }}" alt="">
                                    <span class="open icon6"></span>
                                </button>
                                <div class="dropdown-content" onclick="this.form.submit()">
                                    <a href="#" onclick="document.getElementById('lform').submit();"><img class="img" src="{{ asset('user/img/english.png') }}" alt="">Engleski</a>
                                </div>
                            @else
                                <input hidden name="applocale" value="hr">
                                <button class="dropbtn" value="hr" selected>
                                    <img class="img" src="{{ asset('user/img/english.png') }}" alt="">
                                    <span class="open icon6"></span>
                                </button>
                                <div class="dropdown-content">
                                    <a href="#" onclick="document.getElementById('lform').submit();"><img class="img" src="{{ asset('user/img/croatian.png') }}" alt="">Hrvatski</a>
                                </div>
                            @endif

                        @else
                            <input hidden name="applocale" value="en">
                            <button class="dropbtn" value="hr" selected>
                                <img class="img" src="{{ asset('user/img/croatian.png') }}" alt="">
                                <span class="open icon6"></span>
                            </button>
                            <div class="dropdown-content" onclick="this.form.submit()">
                                <a href="#" onclick="document.getElementById('lform').submit();"><img class="img" src="{{ asset('user/img/english.png') }}" alt="">Engleski</a>
                            </div>
                        @endif
                    </div>
                </form>
            </li>
            <!--
            <li class="nav-element-photo">
                <div class="dropdown">
                    <button class="dropbtn">
                        <img class="img" src="{{ asset('user/img/croatian.png') }}" alt="">
                        <span class="open icon6"></span>
                    </button>
                    <div class="dropdown-content">
                        <a href="#"><img class="img" src="{{ asset('user/img/croatian.png') }}" alt="">Engleski</a>
                    </div>
                </div>

            </li>
            -->
        </ul>
    </nav>
    <div class="topFooter">
        <form class="mainSearch" method="get" action="{{ url('search') }}">
            <input class="input" placeholder="{{ trans('navigation.search') }}" name="q">
            <button class="button" type="submit"><span class="icon"></span></button>
        </form>
        <div class="myList">
            <span class="title">{{ trans('navigation.mojalista') }}</span><br>
            @if(Auth::check())
                <span class="text">{{ trans('navigation.brprograma') }}:</span>
                <span class="number">{!! \App\Http\Controllers\MenuController::getTotalNumberOfFavorites(Auth::user()->id) !!}</span>
            @endif
        </div>
        <a href="{{ url('str/software_list') }}"><button class="plus">+</button></a>
    </div>
</header>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>