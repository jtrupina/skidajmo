<!DOCTYPE html>
<html>
<head>
    <title>Skidajmo.com</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,300' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('user/css/main.css') }}">
</head>
<body>
<div class="contentBox">
    @include('includes/header')
    <div class="mainContent-box">
        <h2> {{ trans('favorites.mojalista') }}:</h2>
        <a href="{{ route('delete.favorite.all') }}"><button class="btn btn-danger">{{ trans('favorites.obrisisve') }}</button></a>
        @foreach($favorites as $favorite)
            <p>{{ $favorite->ime }} | {{ $favorite->verzija }} |
                {{ ($favorite->velicina == -1) ? 'N/A' : number_format($favorite->velicina / 1048576, 2) . 'MB' }} |
                {{ $favorite->autor }} | {{ date('m.d.Y', $favorite->dodano) }} |
                {{ $favorite->skidanja }}
                <a href="{{ route('delete.favorite', [$favorite->id]) }}"><button class="btn btn-danger">{{ trans('favorites.obrisi') }}</button></a>
            </p>
        @endforeach
    </div>
</div>
<div class="leftLogo-box">
    <img class="logo" src="{{ asset('user/img/logo-gore-top.png') }}" alt="">
</div>

{!! \App\Http\Controllers\MenuController::getMenu() !!}

@include('includes/footer')

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src='{{ asset('user/js/swipe/swipe.js') }}'></script>
<script src='{{ asset('user/js/swipe/custom_swipe.js') }}'></script>
</body>
</html>
