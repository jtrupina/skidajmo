<!DOCTYPE html>
<html>
<head>
    <title>Skidajmo.com</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="icon" href="{{ asset('user/img/logo-dolje-desno.png') }}">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,300' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('user/css/main.css') }}">
</head>
<body>
<div class="contentBox">
    @include('includes/header')
    <div class="mainContent-box">
        <div>
            <div class="homeBanner">
                <div class="banner">

                </div>
            </div>
        </div>
        <div class="home-box">
            <div class="header">
                <div class='mainSlider' id='mySwipe'>
                    <div class='swipe-wrap'>
                        @foreach($softwareWeeks as $softwareWeek)
                            <div class="slide">
                                <div class="header">
                                    <span class="title"><a href="{{ url('/' . $softwareWeek->kimeUrl . '/' . $softwareWeek->simeUrl) }}">{{ substr($softwareWeek->ime, 0, 15) . '...' }}</a></span>
                                        <span class="evaluation">
                                            {!! \App\Http\Controllers\MenuController::getStars($softwareWeek->nasaOcjena) !!}
                                        </span>
                                    <br>
                                    <span class="time">{{ trans('home.dodano') }}: {{date('d.m.Y', $softwareWeek->dodano)}}</span>
                                    <span class="version">{{ trans('home.verzija') }} {{$softwareWeek->verzija}}</span>
                                </div>
                                <div class="image-box">
                                    <img class="image" alt="" src="{{ asset('slike/' . $softwareWeek->slika) }}">
                                </div>
                                <div class="text">
                                    {{ substr($softwareWeek->opis, 0, 170) . '...' }}
                                </div>
                                <div class="download-box">
                                    <a href="{{ url('/' . $softwareWeek->kimeUrl . '/' . $softwareWeek->simeUrl) }}">
                                    <button class="download">
                                        <span class="details">DOWNLOAD</span><br>
                                        <span class="size">{{ trans('home.velicina') }}: {{ ($softwareWeek->velicina == -1) ? 'N/A' : number_format($softwareWeek->velicina / 1048576, 2) . 'MB' }}</span>
                                    </button>
                                        </a>

                                </div>
                                @if(Auth::user())
                                    {!! \App\Http\Controllers\FavoritesController::checkFavorite($softwareWeek->software_id) !!}
                                @else
                                    <a href='{{ route('social.login', ['facebook']) }}'><button class='more'>+</button></a>
                                @endif
                            </div>
                        @endforeach
                    </div>
                    <div class="counter content" style='text-align:center;padding-top:20px;'>
                        <ul id='position'>
                            <li class="on a"></li>
                            <li class="a"></li>
                            <li class="a"></li>
                        </ul>
                    </div>
                </div>
            </div>

            @foreach($softwares->slice(0, 4) as $key => $software)
                <div class="col{{$key+1}}">
                    <div class="header">
                        <span class="title"><a href="{{ url('/' . $software->kimeUrl . '/' . $software->simeUrl) }}">{{ substr($software->ime, 0, 15) . '...' }}</a> </span>
                            <span class="evaluation">
                                {!! \App\Http\Controllers\MenuController::getStars($software->nasaOcjena) !!}
                            </span>
                        <br>
                        <span class="time">{{ trans('home.dodano') }}: {{ date('m.d.Y', $software->dodano) }}</span>
                        <span class="version">{{ $software->verzija }}</span>
                    </div>
                    <div class="image-box">
                        <img class="image" alt="" src="{{ asset('slike/' . $software->slika) }}">
                    </div>
                    <div class="a2a_kit">
                        <a class="a2a_button_facebook_like" data-href="{{ url('/' . $software->kimeUrl . '/' . $software->simeUrl) }}" data-layout="standard" data-show-faces="true" data-width="450"></a>
                    </div>

                    <script async src="//static.addtoany.com/menu/page.js"></script>
                    @if(!is_null($software->updated))

                        @if(date('Y-m-d H:i:s') < date('Y-m-d H:i:s', strtotime('+1 day', strtotime($software->updated))))

                            OSVJEŽENO!

                        @endif

                    @endif
                    <div class="text">
                        {{ substr($software->opis, 0, 170) . '...' }}
                    </div>
                    <div class="download-box">
                        <a href="{{ url('/' . $software->kimeUrl . '/' . $software->simeUrl) }}">
                            <button class="download">
                            <span class="image icon3"><img src="{{ asset('user/img/download-ikona-front.png') }}" alt="" class="img"></span>
                            <span class="details">DOWNLOAD</span>
                            <span class="size">{{ trans('home.velicina') }}: {{ ($software->velicina == -1) ? 'N/A' : number_format($software->velicina / 1048576, 2) . 'MB' }}</span>
                            </button>
                        </a>
                    </div>
                    @if(Auth::user())
                        {!! \App\Http\Controllers\FavoritesController::checkFavorite($software->id) !!}
                    @else
                        <a href='{{ route('social.login', ['facebook']) }}'><button class='more'>+</button></a>
                    @endif
                </div>
            @endforeach

            <div>
                <div class="homeBanner">
                    <div class="banner1">

                    </div>
                </div>
            </div>

            @foreach($softwares->slice(4, 4) as $key => $software)
                <div class="col{{$key-3}}">
                    <div class="header">
                        <span class="title"><a href="{{ url('/' . $software->kimeUrl . '/' . $software->simeUrl) }}">{{ substr($software->ime, 0, 15) . '...' }}</a> </span>
                            <span class="evaluation">
                                {!! \App\Http\Controllers\MenuController::getStars($software->nasaOcjena) !!}
                            </span>
                        <br>
                        <span class="time">Dodano: {{ date('m.d.Y', $software->dodano) }}</span>
                        <span class="version">{{ $software->verzija }}</span>
                    </div>
                    <div class="image-box">
                        <img class="image" alt="" src="{{ asset('slike/' . $software->slika) }}">
                    </div>
                    <div class="text">
                        {{ substr($software->opis, 0, 170) . '...' }}
                    </div>
                    <div class="download-box">
                        <a href="{{ url('/' . $software->kimeUrl . '/' . $software->simeUrl) }}">
                            <button class="download">
                                <span class="image icon3"><img src="{{ asset('user/img/download-ikona-front.png') }}" alt="" class="img"></span>
                                <span class="details">DOWNLOAD</span>
                                <span class="size">Veličina: {{ ($software->velicina == -1) ? 'N/A' : number_format($software->velicina / 1048576, 2) . 'MB' }}</span>
                            </button>
                        </a>
                    </div>
                    @if(Auth::user())
                        {!! \App\Http\Controllers\FavoritesController::checkFavorite($software->id) !!}
                    @else
                        <a href='{{ route('social.login', ['facebook']) }}'><button class='more'>+</button></a>
                    @endif
                </div>
            @endforeach
            <div style="text-align: center">
                {!! $softwares->render() !!}
            </div>

            </div>
            <div>
                <div class="homeBanner">
                    <div class="banner">

                    </div>
                </div>
            </div>



        </div>




</div>
<div class="leftLogo-box">
    <img class="logo" src="{{ asset('user/img/logo-gore-top.png') }}" alt="">
</div>

{!! \App\Http\Controllers\MenuController::getMenu() !!}

@include('includes/footer')

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src='{{ asset('user/js/swipe/swipe.js') }}'></script>
<script src='{{ asset('user/js/swipe/custom_swipe.js') }}'></script>
</body>
<div class="cookies"  id="cookies" style="text-align:center">
    <p style="color:white; padding: 20px;"> Radi poboljšanja korisničkog iskustva koristimo kolačiće. Nastavkom korištenja portala skidajmo.com potvrđuješ da si upoznat sa time.<p>
        <button style="margin-top:-20px; background-color:#00acee; color: white; border: 1px solid white; width: 150px; height: 35px; border-radius: 5px; cursor: pointer" onclick="closeCookies()">OK</button>
</div>

<script>
    function closeCookies() {

        $("#cookies").fadeOut("slow");

        $.ajax({
            type: "POST",
            url: 'http://localhost:8000/api/v1/cookie',
            success: function() {}
        });
    }
</script>
</html>
