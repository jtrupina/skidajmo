@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Popis programa sa mrtvim linkovima</h1>
        <a href="{{url('admin/deadlinks/check')}}" class="btn btn-info" target="_blank">Pokreni provjeru linkova</a>
        <hr>
        @if (session('message'))
            <div class="alert alert-danger">
                {{ session('message') }}
            </div>
        @endif
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>ID</th>
                <th>Ime programa</th>
                <th>Path</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($softwares as $software)
                <tr>
                    <td>{{ $software->id }}</td>
                    <td>{{ $software->ime }}</td>
                    <td>
                        <form action="{{ route('deadlinks.update', [$software->id]) }}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="text" name="path" style="width: 300px">
                            <button type="submit" class="btn btn-success">Ažuriraj link</button>
                        </form>
                        <form action="{{ route('deadlinks.delete', [$software->id]) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-danger">Obriši</button>
                        </form>
                    </td>

                </tr>
            @endforeach

            </tbody>

        </table>

    </div>
@endsection