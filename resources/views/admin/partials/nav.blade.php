<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ url('admin/home') }}">Admin Zona</a>
    </div>
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa fa-envelope"></i>
                <b class="caret"></b></a>
            <ul class="dropdown-menu message-dropdown">

            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
                {{ Auth::user()->name }}<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{ url('logout') }}"><i class="fa fa-fw fa-power-off"></i>Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="active">
                <a href="{{ url('admin/home') }}"><i class="fa fa-fw fa-dashboard"></i> Početna</a>
            </li>
            <li>
                <a href="{{ url('admin/kategorije') }}"><i class="fa fa-fw fa-table"></i> Kategorije</a>
            </li>
            <li>
                <a href="{{ url('admin/software') }}"><i class="fa fa-fw fa-dashboard"></i> Software</a>
            </li>
            <li>
                <a href="{{ url('admin/korisnici') }}"><i class="fa fa-fw fa-dashboard"></i> Korisnici</a>
            </li>
            <li>
                <a href="{{ url('admin/week/programs') }}"><i class="fa fa-fw fa-dashboard"></i> Programi tjedna</a>
            </li>
            <li>
                <a href="{{ url('admin/deadlinks') }}"><i class="fa fa-fw fa-dashboard"></i> Mrtvi linkovi</a>
            </li>
            <li>
                <a href="{{ url('admin/korisnici-software') }}"><i class="fa fa-fw fa-dashboard"></i> Software od korisnika</a>
            </li>
            <li>
                <a href="{{ url('admin/content') }}"><i class="fa fa-fw fa-dashboard"></i> Uredi sadržaj</a>
            </li>
        </ul>
    </div>
</nav>