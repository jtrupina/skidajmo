@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Update User</h1>
        <form method="POST" action="{{ route('admin.kategorije.update', [$category->id]) }}">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="form-group">
                <label for="name">Naziv kategorije (HR):</label>
                <input type="text" name="imeKategorije" class="form-control" value="{{ $category->imeKategorije }}">
            </div>
            <div class="form-group">
                <label for="email">Naziv kategorije (ENG):</label>
                <input type="text" name="nameKategorije" class="form-control" value="{{ $category->nameKategorije }}">
            </div>
            <div class="form-group">
                <label for="city">Ime Url:</label>
                <input type="text" name="imeUrl" class="form-control" value="{{ $category->imeUrl }}">
            </div>
            <div class="form-group">
                <label for="country">Name Url:</label>
                <input type="text" name="nameUrl" class="form-control" value="{{ $category->nameUrl }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Save</button>
            </div>
        </form>
    </div>
@endsection