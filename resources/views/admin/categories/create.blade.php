@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Kreiraj kategoriju</h1>
        <form method="POST" action="{{ url('admin/kategorije') }}">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="form-group">
                <label for="imeKategorije">Naziv kategorije (HR):</label>
                <input type="text" name="imeKategorije" class="form-control">
            </div>
            <div class="form-group">
                <label for="nameKategorije">Naziv kategorije (ENG):</label>
                <input type="text" name="nameKategorije" class="form-control">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Kreiraj</button>
            </div>
        </form>
    </div>
@endsection