@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Kategorija pogled</h1>

        <form class="form-horizontal">
            <div class="form-group">
                <label for="id" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="id" placeholder="{{$category->id}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Naziv kategorije (HR)</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" placeholder="{{$category->imeKategorije}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Naziv kategorije (ENG)</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="email" placeholder="{{$category->nameKategorije}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="city" class="col-sm-2 control-label">Friendly url (HR)</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="city" placeholder="{{$category->imeUrl}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="city" class="col-sm-2 control-label">Friendly url (ENG)</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="city" placeholder="{{$category->nameUrl}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <a href="{{ url('admin/kategorije')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </form>

    </div>
@endsection