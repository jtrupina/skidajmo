@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Kategorije</h1>
        <a href="{{url('admin/kategorije/create')}}" class="btn btn-success">Kreiraj kategoriju</a>
        <hr>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>ID</th>
                <th>Naziv kategorije (HR)</th>
                <th>Naziv kategorije (ENG)</th>
                <th colspan="3">Akcije</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->imeKategorije }}</td>
                    <td>{{ $category->nameKategorije }}</td>
                    <td><a href="{{url('admin/kategorije',$category->id)}}" class="btn btn-primary">Vidi</a></td>
                    <td><a href="{{route('admin.kategorije.edit',$category->id)}}" class="btn btn-warning">Ažuriraj</a></td>
                    <td>
                        <form action="{{ route('admin.kategorije.destroy', [$category->id]) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-danger">Obriši</button>
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>
        <div style="text-align: center">
            {!! $categories->render() !!}
        </div>

    </div>
@endsection