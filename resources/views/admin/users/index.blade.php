@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Korisnici</h1>
        <a href="{{url('admin/korisnici/create')}}" class="btn btn-success">Kreiraj korisnika</a>
        <hr>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>ID</th>
                <th>Ime</th>
                <th>Prezime</th>
                <th>Korisničko ime</th>
                <th>Datum registracije</th>
                <th colspan="3">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->ime }}</td>
                    <td>{{ $user->prezime }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->registriran }}</td>
                    <td><a href="{{url('admin/korisnici',$user->id)}}" class="btn btn-primary">Vidi</a></td>
                    <td><a href="{{route('admin.korisnici.edit',$user->id)}}" class="btn btn-warning">Ažuriraj</a></td>
                    <td>
                        <form action="{{ route('admin.korisnici.destroy', [$user->id]) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-danger">Obriši</button>
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>
        <div style="text-align: center">
            {!! $users->render() !!}
        </div>

    </div>
@endsection