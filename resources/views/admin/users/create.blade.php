@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Kreiraj korisnika</h1>
        <form method="POST" action="{{ url('admin/korisnici') }}">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="form-group">
                <label for="ime">Ime:</label>
                <input type="text" name="ime" class="form-control">
            </div>
            <div class="form-group">
                <label for="prezime">Prezime:</label>
                <input type="text" name="prezime" class="form-control">
            </div>
            <div class="form-group">
                <label for="username">Korisničko ime:</label>
                <input type="text" name="username" class="form-control">
            </div>
            <div class="form-group">
                <label for="password">Lozinka:</label>
                <input type="password" name="password" class="form-control">
            </div>
            <div class="form-group">
                <label for="role">Rola:</label>
                <select name="dozvole" class="form-control">
                    <option value="">Odaberi rolu</option>
                    <option value="1">User</option>
                    <option value="0">Administrator</option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Kreiraj</button>
            </div>
        </form>
    </div>
@endsection