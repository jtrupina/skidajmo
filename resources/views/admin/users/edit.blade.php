@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Ažuriraj korisnika</h1>
        <form method="POST" action="{{ route('admin.korisnici.update', [$user->id]) }}">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="form-group">
                <label for="name">Ime:</label>
                <input type="text" name="ime" class="form-control" value="{{ $user->ime }}">
            </div>
            <div class="form-group">
                <label for="email">Prezime:</label>
                <input type="text" name="prezime" class="form-control" value="{{ $user->prezime }}">
            </div>
            <div class="form-group">
                <label for="city">Korisničko ime:</label>
                <input type="text" name="username" class="form-control" value="{{ $user->username }}">
            </div>
            <div class="form-group">
                <label for="role">Rola:</label>
                <select name="dozvole" class="form-control">
                    <option value="">Choose role</option>
                    @if($user->role == 1)
                        <option value="1" selected>User</option>
                        <option value="0">Administrator</option>
                    @else
                        <option value="1">User</option>
                        <option value="0" selected>Administrator</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Ažuriraj</button>
            </div>
        </form>
    </div>
@endsection