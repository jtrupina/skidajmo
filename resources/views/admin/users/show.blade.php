@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Korisnik pogled</h1>

        <form class="form-horizontal">
            <div class="form-group">
                <label for="id" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="id" placeholder="{{$user->id}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Ime</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" placeholder="{{$user->ime}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Prezime</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="email" placeholder="{{$user->prezime}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="city" class="col-sm-2 control-label">Korisničko ime</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="city" placeholder="{{$user->username}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="country" class="col-sm-2 control-label">Rola</label>
                @if($user->dozvole == 0)
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="country" placeholder="Administrator" readonly>
                    </div>
                    @else
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="country" placeholder="User" readonly>
                    </div>
                    @endif

            </div>
            <div class="form-group">
                <label for="birth_date" class="col-sm-2 control-label">Datum registracije</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="birth_date" placeholder="{{$user->registriran}}" readonly>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <a href="{{ url('admin/korisnici')}}" class="btn btn-primary">Natrag</a>
                </div>
            </div>
        </form>

    </div>
@endsection