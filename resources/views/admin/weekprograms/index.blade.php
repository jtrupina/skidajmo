@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Programi tjedna - sortirano od najnovijeg prema najstarijem</h1>
        <h2>Trenutni datum i tjedan: {{ date('Y-m-d') }} | Tjedan: {{ date('W', time()) }}</h2>
        <a href="{{url('admin/software')}}" class="btn btn-success">Dodaj novi program tjedna</a>
        <hr>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>Ime programa</th>
                <th>Broj tjedna</th>
                <th>Dodano:</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($softwareWeeks as $software)
                <tr>
                    <td>{{ $software->ime }}</td>
                    <td>{{ date('W', $software->tjedan) }}.</td>
                    <td>{{ date('Y-m-d H:i:s', $software->tjedan) }}</td>
                </tr>
            @endforeach

            </tbody>

        </table>

    </div>
@endsection