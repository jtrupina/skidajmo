<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Admin's dashboard">
    <meta name="author" content="Team Bastion">

    <title>Skidajmo.com - admin home</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('admin/css/sb-admin.css') }}" rel="stylesheet">

    <!-- Lightbox CSS -->
    <link href="{{ asset('admin/css/lightbox.min.css') }}" rel="stylesheet">

    <!-- Morris CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

    <!-- Custom Fonts -->
    <link href="{{ asset('admin/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- WYSIWYG -->
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div id="wrapper">
    @include('admin.partials.nav')

    <div id="page-wrapper">
        <div class="container-fluid">
            <h1>Uredi sadržaj na stranici: O nama</h1>
            <form method="post" action="{{ url('admin/content') }}">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <textarea id="mytextarea" name="contents" rows="50">{{ $content->content }}</textarea>
                <button type="submit">Spremi promjene</button>
            </form>
        </div>
    </div>

</div>

<!-- jQuery -->
<script src="{{ asset('admin/js/jquery.min.js') }}"></script>
<!-- Lightbox -->
<script src="{{ asset('admin/js/lightbox.min.js') }}"></script>
<!-- Morris -->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
</body>
</html>