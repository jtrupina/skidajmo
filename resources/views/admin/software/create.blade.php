@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Dodaj software</h1>
        @if (session('message'))
            <div class="alert alert-danger">
                {{ session('message') }}
            </div>
        @endif
        <form method="POST" action="{{ url('admin/software') }}" enctype="multipart/form-data">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="form-group">
                <label for="ime">Ime programa:</label>
                <input type="text" name="ime" class="form-control">
            </div>
            <div class="form-group">
                <label for="kategorija">Kategorija:</label>
                <select name="kategorija" class="form-control">
                    <option value="">Odaberi kategoriju</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->imeKategorije }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="autor">Autor:</label>
                <input type="text" name="autor" class="form-control">
            </div>
            <div class="form-group">
                <label for="username">Homepage:</label>
                <input type="text" name="homepage" class="form-control">
            </div>
            <div class="form-group">
                <label for="username">Verzija:</label>
                <input type="text" name="verzija" class="form-control">
            </div>
            <div class="form-group">
                <label for="licenca">Licenca:</label>
                <select name="licenca" id="licenca" class="form-control">
                    <option value="Freeware" selected="selected">Freeware</option>
                    <option value="Shareware">Shareware</option>
                    <option value="Demo">Demo</option>
                    <option value="Trial">Trial</option>
                    <option value="Adware">Adware</option>
                </select>
            </div>
            <div class="form-group">
                <label for="platforma">Platforma:</label>
                <select name="platforma" id="platforma" class="form-control">
                    <option value="Linux">Linux</option>
                    <option value="Windows x86">Windows x86</option>
                    <option value="Win 7,Win 8, Win 10" selected="selected">Win 7,Win 8, Win 10</option>
                    <option value="Windows x64">Windows x64</option>
                    <option value="Macintosh">Macintosh</option>
                </select>
            </div>
            <div class="form-group">
                <label for="sucelje">Sučelje:</label>
                <select name="sucelje" id="platforma" class="form-control">
                    <option value="1">1/5</option>
                    <option value="2">2/5</option>
                    <option value="3">3/5</option>
                    <option value="4">4/5</option>
                    <option value="5">5/5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="mogucnosti">Mogućnosti:</label>
                <select name="mogucnosti" id="platforma" class="form-control">
                    <option value="1">1/5</option>
                    <option value="2">2/5</option>
                    <option value="3">3/5</option>
                    <option value="4">4/5</option>
                    <option value="5">5/5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="jednostavnost">Jednostavnost:</label>
                <select name="jednostavnost" id="platforma" class="form-control">
                    <option value="1">1/5</option>
                    <option value="2">2/5</option>
                    <option value="3">3/5</option>
                    <option value="4">4/5</option>
                    <option value="5">5/5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="nasaOcjena">Naša ocjena:</label>
                <select name="nasaOcjena" id="platforma" class="form-control">
                    <option value="1">1/5</option>
                    <option value="2">2/5</option>
                    <option value="3">3/5</option>
                    <option value="4">4/5</option>
                    <option value="5">5/5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="preporuka">Preporuka:</label>
                <input type="checkbox" value="1" class="form-control" name="preporuka">
            </div>
            <div class="form-group">
                <label for="path">Path:</label>
                <input type="text" class="form-control" name="path">
            </div>
            <div class="form-group">
                <label for="file">Upload file (ako nema path):</label>
                <input type="file" class="form-control" name="dl_file">
            </div>
            <div class="form-group">
                <label for="path">Opis HR:</label>
                <textarea name="opis" class="form-control" rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="path">Opis ENG:</label>
                <textarea name="opisEn" class="form-control" rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="file">Slika:</label>
                <input type="file" class="form-control" name="file" size="80">
            </div>
            <div class="form-group">
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker6'>
                        <input type='text' class="form-control" name="updated" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="path">Skidanja:</label>
                <input type="text" class="form-control" name="skidanja">
            </div>
                <div class="form-group">
                    <label for="path">Veličina (U BAJTOVIMA):</label>
                    <input type="text" class="form-control" name="velicina">
                </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Dodaj</button>
            </div>
        </form>
    </div>
@endsection