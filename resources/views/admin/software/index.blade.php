@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Software</h1>
        <a href="{{url('admin/software/create')}}" class="btn btn-success">Dodaj software</a>
        <form method="post" action="{{ url('admin/software/search') }}">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <input type="search" placeholder="upiši ime software..." name="ime">
            <input type="submit" value="Pretraži..." class="btn-info">
        </form>

        <hr>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>ID</th>
                <th>Ime programa</th>
                <th colspan="3">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($softwares as $software)
                <tr>
                    <td>{{ $software->id }}</td>
                    <td>{{ $software->ime }}</td>
                    <td><a href="{{route('admin.software.edit',$software->id)}}" class="btn btn-warning">Ažuriraj</a></td>
                    <td>
                        <form action="{{ route('admin.software.destroy', [$software->id]) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-danger">Obriši</button>
                        </form>
                    </td>
                    <td>
                        <form action="{{ route('week.program.add', [$software->id]) }}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="text" name="ime" value="{{ $software->ime }}" hidden>
                            <button type="submit" class="btn btn-primary">Označi kao program tjedna</button>
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>
        @if($searching === true)
            @else
        <div style="text-align: center">
            {!! $softwares->render() !!}
        </div>
            @endif

    </div>
@endsection