@extends('admin.default.layout')

@section('content')
    <div class="container">
        <h1>Programi objavljeni od korisnika</h1>
        <hr>
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>Ime programa</th>
                <th>Email korisnika</th>
                <th style="width: 500px">Opis programa</th>
                <th>Adresa softvera</th>
                <th>Datum i vrijeme</th>
                <th colspan="2">Akcije</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($userSoftwares as $userSoftware)
                <tr>
                    <td>{{ $userSoftware->name }}</td>
                    <td>{{ $userSoftware->email }}</td>
                    <td>{{ $userSoftware->description }}</td>
                    <td>{{ $userSoftware->address_software }}</td>
                    <td>{{ $userSoftware->date }}</td>
                    @if($userSoftware->approved == 0)
                        <td><a href="{{ route('user.software.accept', [$userSoftware->id]) }}" class="btn btn-success">Prihvati</a></td>
                    @else
                        <td><button class="btn btn-info">Prihvaćen</button></td>
                    @endif
                    <td>
                        <form action="{{ route('user.software.delete', [$userSoftware->id]) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-danger">Obriši</button>
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>
        <div style="text-align: center">
            {!! $userSoftwares->render() !!}
        </div>

    </div>
@endsection