<nav class="nav-left">
    <ul class="nav-list">
        <li class="nav-titleElem">
            <span class="icon7 menu"></span>
            <a class="nav-title">
                {{ trans('home.kategorije') }}
            </a>
                    <span class="socialIcon-Element">
                        <span class="socialIcon-Icon4"><span class="icon"></span></span>
                        <span class="text">{!! \App\Http\Controllers\MenuController::getNumberOfFbFans() !!}</span>
                    </span>
                    <span class="socialIcon-Element" >
                        <span class="socialIcon-Icon5"><span class="icon"></span></span>
                        <span class="text1">Follow us</span>
                    </span>
        </li>
    </ul>
    <ul class="nav-list">
        <li class="nav-element">
            <span class="icon home"></span>
            <a class="nav-text" href="{{ url('/') }}">
                {{ trans('home.naslovnica') }}
            </a>
            <span class="navCounter">{!! \App\Http\Controllers\MenuController::getTotalNumberOfSoftware() !!}</span>
        </li>
    </ul>
    @foreach($categories as $category)
        <ul class="nav-list">
            <li class="nav-element">
                <span class="{{ $category->css_icon }}"></span>
                <?php $url = App::getLocale() == 'hr' ? $category->imeUrl : $category->nameUrl ?>
                <a class="nav-text" href="{{ url('/' . $url) }}">
                    {{ strtoupper(App::getLocale() == 'hr' ? $category->imeKategorije : $category->nameKategorije) }}
                </a>
                <span class="navCounter">{{ $category->softwares->count() }}</span>
            </li>
        </ul>
    @endforeach

</nav>
<div class="question-box">
    <span class="icon menu"></span>
    <h1 class="question-title">{{ trans('home.pitanja') }}?</h1>
    <span class="icon chat"></span>
    <ul class="question-list">
        {!! \App\Http\Controllers\MenuController::getFiveMostRecentQuestions() !!}
    </ul>
</div>
<div class="recommendation-box">
    <span class="icon menu"></span>
    <h1 class="recommendation-title">{{trans('home.popularnisoftver')}}</h1>
    <span class="icon chat"></span>
    <ul class="recommendation-list">
        @foreach($popularSoftwares as $software)

            <li class="recommendation-item"><span class="recommendation-text"><a href="{{ url('/' . $software->kimeUrl . '/' . $software->simeUrl) }}">{{ substr($software->ime, 0, 65) . '...' }}</a> </span></li>

        @endforeach
    </ul>
</div>
<div class="recommendation-box">
    <span class="icon menu"></span>
    <h1 class="recommendation-title">{{ trans('home.preporuka') }}</h1>
    <span class="icon chat"></span>
    <ul class="recommendation-list">
        @foreach($softwares as $software)

            <li class="recommendation-item"><span class="recommendation-text"><a href="{{ url('/' . $software->kimeUrl . '/' . $software->simeUrl) }}">{{ substr($software->ime, 0, 65) . '...' }}</a> </span></li>

        @endforeach
    </ul>
</div>