<!DOCTYPE html>
<html>
<head>
    <title>Skidajmo.com</title>
    <meta charset="UTF-8">
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $sw->ime }}" />
    <meta property="og:description" content="{{ fb_desc_process($sw->opis) }}" />
    <meta property="og:image" content="{{ asset('slike/' . $sw->slika) }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,300' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('user/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('user/css/image-popup.css') }}">
</head>
<body>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="contentBox">
    @include('includes/header')
    <div class="mainContent-box">
        <div class="details-title">
            <h1 class="title">{{ $sw->ime }}</h1>
        </div>
        <div class="topBanner">
            <div class="banner">

            </div>
        </div>
        <div class="details-box">
            <div class="details-img">
                <img id="myImg" class="image" alt="" src="{{ asset('slike/' . $sw->slika) }}" data-original="{{ asset('slike/originals/' . $sw->slika) }}">
            </div>
            <!-- The Modal -->
            <div id="myModal" class="modal">

                <!-- The Close Button -->
                <span class="close" onclick="document.getElementById('myModal').style.display='none'">&times;</span>

                <!-- Modal Content (The Image) -->
                <img class="modal-content" id="img01">

                <!-- Modal Caption (Image Text) -->
                <div id="caption"></div>
            </div>
            <div class="mainInformation-box">
                <ul class="list">
                    <li class="element">
                        <a href="{{ $sw->homepage }}" target="_blank" class="text"><span class="title">{{ trans('details.autor') }}: </span> {{ $sw->autor }}</a>
                    </li>
                    <li class="element">
                        <a class="text"><span class="title">{{ trans('details.dodano') }}: </span> {{ date('m.d.Y h:i:s', $sw->dodano) }}</a>
                    </li>
                    <li class="element">
                        <a class="text"><span class="title">{{ trans('details.verzija') }}: </span> {{ $sw->verzija }}</a>
                    </li>
                    <li class="element">
                        <a class="text"><span class="title">{{ trans('details.velicina') }}: </span> {{ ($sw->velicina == -1) ? 'N/A' : number_format($sw->velicina / 1048576, 2) . 'MB' }}</a>
                    </li>
                    <li class="element">
                        <a class="text"><span class="title">{{ trans('details.preuzimanje') }}: </span> {{ $sw->skidanja }}</a>
                    </li>
                    <li class="element">
                        <a class="text"><span class="title">{{ trans('details.licenca') }}: </span> {{ $sw->licenca }}</a>
                    </li>
                    <li class="element">
                        <a class="text"><span class="title">{{ trans('details.platforma') }}: </span> {{ $sw->platforma }}</a>
                    </li>
                    <li class="element">
                        <a class="text"><span class="title">{{ trans('details.kategorija') }}: </span> {{ $c->imeKategorije }}</a>
                    </li>
                </ul>
                @if ($sw->preporuka)

                    {{ trans('details.nasapreporuka') }}

                    TU IDE ONA SLIKA

                @endif
            </div>
            <div class="banner-box1">
                <img class="banner1" alt="" src="">
            </div>
            <div class="otherInformation-box">
                <h1 class="share">{{ trans('details.podijeli') }}</h1>
                <div class="fb-like" data-href="{{ Request::url() }}" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>

                <!-- Your share button code -->
                <div class="fb-share-button"
                     data-href="{{ Request::url() }}"
                     data-layout="button_count">
                </div>

                <p class="last" style="padding-top:10px;">
                    <a href="https://twitter.com/share" class="twitter-share-button" data-via="skidajmo">Tweet</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>



                </p>

                <script src="https://apis.google.com/js/platform.js" async defer></script>
                <g:plus action="share"></g:plus>
            </div>
            <div class="details-text">
                @if(App::getLocale('applocale') == 'en')
                    {{ $sw->opisEn }}
                @else
                    {{ $sw->opis }}
                @endif
            </div>
            <div class='evaluation-box1'>
                <div class='color'>
                    <a class='type'>{{ trans('details.vasaocjena') }}</a>
            {!! \App\Http\Controllers\MarksController::getMarks($sw->id) !!}
                    <div class="fb-comments" data-href="" data-numposts="5" data-width="485"></div>
        </div>
        <div class="rightBanner-box">
            <div class="banner-box">
                <div class="banner"></div>
            </div>
            <div class="download-box">
                <a href="{{ route('download.software', array($sw->id)) }}"> <button class="download">
                    <span class="image">-></span>
                    <span class="details">DOWNLOAD</span>
                    <span class="size">{{ trans('details.velicina') }}: {{ ($sw->velicina == -1) ? 'N/A' : number_format($sw->velicina / 1048576, 2) . 'MB' }}</span>
                </button></a>
                @if(Auth::user())
                    {!! \App\Http\Controllers\FavoritesController::checkFavorite($sw->id) !!}
                @else
                    <a href='{{ route('social.login', ['facebook']) }}'><button class='more'>+</button></a>
                @endif

                <div class="information">
                    <span class="image">-></span>
                    <a href="{{ route('download.software.mirror', array($sw->id)) }}"> <span class="text1">DOWNLOAD MIROR: </span></a>
                    <span class="text2">SERVER SKIDAJMO </span>
                </div>
            </div>
            <div class="evaluation-box">
                <span class="title">{{ trans('details.naseocjene') }}: </span>
                <span class="name">{{ $sw->ime }}</span>
                <ul class="list">
                    <li class="element">
                        <a class="type">{{ trans('details.sucelje') }}</a>
                        <div class="evaluation">
                            {!! \App\Http\Controllers\MenuController::getStars($sw->sucelje) !!}
                        </div>
                    </li>
                    <li class="element">
                        <a class="type">{{ trans('details.mogucnosti') }}</a>
                        <div class="evaluation">
                            {!! \App\Http\Controllers\MenuController::getStars($sw->mogucnosti) !!}

                        </div>
                    </li>
                    <li class="element">
                        <a class="type">{{ trans('details.jednostavnost') }}</a>
                        <div class="evaluation">
                            {!! \App\Http\Controllers\MenuController::getStars($sw->jednostavnost) !!}
                        </div>
                    </li>
                    <li class="element">
                        <a class="type">{{ trans('details.nasaocjena') }}</a>
                        <div class="evaluation">
                            {!! \App\Http\Controllers\MenuController::getStars($sw->nasaOcjena) !!}
                        </div>
                    </li>
                </ul>
                <div class="footer">
                    <span class="image"></span>
                    <p class="text">
                        Skidajmo.com je skenirao
                        <span class="programName">Google Chrome</span>
                        sa komercijalnim antivirusnim programom i kao takav je
                        <span class="percent">100%</span>
                        siguran za Vas.
                    </p>
                </div>
            </div>
            <div class="news-box">
                <span class="icon menu"></span>
                <h1 class="question-title">{{ trans('details.najnoviji') }}</h1>
                <span class="icon chat"></span>
                <ul class="question-list">
                    @foreach($newSoftwares as $software)

                        <li class="question-item"><span class="question-text"><a href="{{ url('/' . $software->kimeUrl . '/' . $software->simeUrl) }}">{{ substr($software->ime, 0, 65) . '...' }}</a></span></li>

                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="leftLogo-box">
    <img class="logo" src="{{ asset('user/img/logo-gore-top.png') }}" alt="">
</div>

{!! \App\Http\Controllers\MenuController::getMenu() !!}

@include('includes/footer')

<script src="{{ asset('user/js/marks.js') }}"></script>
        <script src="{{ asset('user/js/image-popup.js') }}"></script>

</body>
</html>
