<!DOCTYPE html>
<html>
<head>
    <title>Skidajmo.com</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('user/img/logo-dolje-desno.png') }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,300' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('user/css/main.css') }}">
</head>
<body>
<div class="contentBox">
    @include('includes/header')
    <div class="mainContent-box" id="content">
        <div class="topBanner">
            <div class="banner">

            </div>
        </div>
        <div class="addTitle">
            <h1 class="title">Dodajte Vaš softver na skidajmo.com</h1>
            <p class="text">
                Znate za aplikaciju koja se još ne nalazi u našem repozitoriju softvera?<br>
                Ukoliko ste razvili neku zanimljivu aplikaciju, dodajte istu na skidajmo.com.
            </p>
        </div>
        @if(Session::has('fail'))

            <span>{{ Session::get('fail') }}</span>
        @endif
        <form class="addSoftver-box" method="post" action="{{ route('dodaj.software') }}">
            <input class="input" placeholder="Naziv programa" name="name">
            <input class="input" placeholder="Vaš email" name="email">
            <input class="input" placeholder="Web adresa programa" name="address_software">
            <textarea class="textarea" placeholder="Kratak opis programa" name="description"></textarea>
            <input hidden name="num1" value="{{ $num1 }}">
            <input hidden name="num2" value="{{ $num2 }}">
            <input class="input"  name="sum" placeholder="Spam provjera: Koliko je {{ $num1 }} + {{ $num2 }}?">
            <button class="button" type="submit"><span id="download" class="image icon3"><img src="{{ asset('user/img/download-ikona-front.png') }}" alt="" class="img"></span>DODAJ PROGRAM</button>
            @if(Session::has('success'))
                <span class="message">Program uspješno dodan. Hvala!</span>
            @endif
        </form>
        <div class="warningText-box">
            <p class="text">
                OBJAVIT ĆEMO SAMO BESPLATNE APLIKACIJE. UKOLIKO JE VAŠA APLIKACIJA SHAREWARE
                ILI TRIAL VERZIJA MOLIMO POGLEDAJTE DONJE UVJETE PROMIDŽBE NA SKIDAJMO.COM
            </p>
        </div>
        <div class="sendMessage-box">
            <h1 class="title">Vaša komercijalna aplikacija na Skidajmo.com?</h1>
            <p class="cost">500 kn / jednokratno</p>
            <p class="message">
                Vašu aplikaciju mjesečno vidi preko 600 000 korisnika<br>
                Vaša aplikacija smještena je na našim serverima<br>
                Povećajte prodaju svojih aplikacija na najposjećenijem download portalu na Balkanu
            </p>
            <button class="button">Pošaljite nam Vašu aplikaciju</button>
        </div>
        <div>
            <div class="bottomBanner">
                <div class="banner">

                </div>
            </div>
        </div>
    </div>
</div>
<div class="leftLogo-box">
    <img class="logo" src="{{ asset('user/img/logo-gore-top.png') }}" alt="">
</div>
{!! \App\Http\Controllers\MenuController::getMenu() !!}


@include('includes/footer')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src='{{ asset('user/js/swipe/swipe.js') }}'></script>
<script src='{{ asset('user/js/swipe/custom_swipe.js') }}'></script>
<script src='{{ asset('user/js/menu.js') }}'></script>
</body>
</html>
