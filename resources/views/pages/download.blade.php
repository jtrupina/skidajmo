<!DOCTYPE html>
<html>
<head>
    <title>Skidajmo.com</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="icon" href="{{ asset('user/img/logo-dolje-desno.png') }}">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,300' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('user/css/main.css') }}">
</head>
<body>
<div class="contentBox">
    @include('includes/header')
    <div class="mainContent-box" id="content">
        <div class="topAddSense">
            <div class="addSense">

            </div>
        </div>
        <div class="topBanner">
            <div class="banner">

            </div>
        </div>
        <div class="downloadTitle">
            <p class="text">
                Preuzimanje
                <span class="title">{{$software->ime}}</span>
                će se automatski pokrenuti ...
            </p>
            <p class="description">
                Ukoliko download ne započne automatski, kliknite
                <a class="link" href="{{ route('download.software.path', [$software->id]) }}">ovdje</a>
                da biste započeli preuzimanje ručno.
            </p>
        </div>
        <div id="download"></div>
        <script type="text/javascript">

            window.setTimeout(function() {
                $("#download").html('<iframe src="{{ route('download.software.path', [$software->id]) }}" width="0" height="0" style="display:none"></iframe>');
            }, 3000);

        </script>
        <div class="addSense-row">
            <div class="col1">

            </div>
            <div class="col2">

            </div>
        </div>

        <div class="download-box">
            <div class="download">
                <ul class="list">
                    @foreach($softwares as $key => $software)
                        <li class="item item{{$key+1}}">
                            <div class="col1">
                                <span class="text">{{$key+1}}.</span>
                            </div>
                            <div class="col2">
                                <img class="image" alt="" src="{{ asset('slike/' . $software->slika) }}">
                                <h3 class="title">{{$software->ime}}</h3>
                                <div class="version">Verzija: {{$software->verzija}}</div>
                            </div>
                            <div class="col3">
                                <p class="size">Veličina: {{ ($software->velicina == -1) ? 'N/A' : number_format($software->velicina / 1048576, 2) . 'MB' }}</p>
                                <p class="download">Preuzimanja: {{$software->skidanja}}</p>
                            </div>
                            <div class="col4">
                                <div class="evaluation">
                                    {!! \App\Http\Controllers\MenuController::getStars($software->nasaOcjena) !!}
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="leftLogo-box">
    <img class="logo" src="{{ asset('user/img/logo-gore-top.png') }}" alt="">
</div>
{!! \App\Http\Controllers\MenuController::getMenu() !!}

<footer class="mainFooter-box">
    <div class="footerList">
        <div class="col1">
            <h1 class="title">Korisni linkovi</h1>
            <ol class="linkList">
                <li class="linkItem"><a href="">Filesfreeware.com</a></li>
                <li class="linkItem"><a href="">Besplatnestvari.biz</a></li>
                <li class="linkItem"><a href="">HD Televizija</a></li>
                <li class="linkItem"><a href="">Racunalo.com</a></li>
                <li class="linkItem"><a href="">CroSatelite.com</a></li>
                <li class="linkItem"><a href="">linuxzasve</a></li>
            </ol>
        </div>
        <div class="col2">
            <h1 class="title">Vaš oglas ovdje</h1>
            <div class="image"></div>
        </div>
        <div class="col3">
            <h1 class="title">Facebook</h1>

        </div>
        <div class="col4">
            <h1 class="title">Statistika</h1>
            <ol class="statisticList">
                <li class="statisticItem">Programa: <span class="number">1475</span></li>
                <li class="statisticItem">Kategorija: <span class="number">15</span></li>
                <li class="statisticItem">Ukupna veličina: <span class="number">30.78 GB</span></li>
                <li class="statisticItem">Korisnika online: <span class="number">1698</span></li>
            </ol>
            <div class="image"></div>
        </div>
    </div>
    <div class="copyright-box">
        <div class="text-box">
            <p class="text1">Copyright @ by Ivica Kutnjak | Skidajmo.com 2007.-2004. | RSS Feed | Skidajmo.com koristi i preporučuje AVALON hosting | Web by idip.hr</p>
            <p class="text2">Najbolji besplatni programi i igrice na jednom mjestu | Free software in one place. | Free Software and Games.</p>
        </div>
        <div class="socialIcon-box">
            <div class="socialIcon-List">
                <div class="socialIcon-Element">
                    <div class="socialIcon-Icon1"><span class="icon"></span></div>
                </div>
                <div class="socialIcon-Element">
                    <div class="socialIcon-Icon2"><span class="icon"></span></div>
                </div>
                <div class="socialIcon-Element">
                    <div class="socialIcon-Icon3"><span class="icon"></span></div>
                </div>
                <div class="socialIcon-Element">
                    <div class="socialIcon-Icon4"><span class="icon"></span></div>
                </div>
                <div class="socialIcon-Element">
                    <div class="socialIcon-Icon5"><span class="icon"></span></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src='js/swipe/swipe.js'></script>
<script src='js/swipe/custom_swipe.js'></script>
<script src='js/menu.js'></script>
<script>
    $(".item1" ).mouseover(function() {
        $(".evaluation_point1").css("color", "#eaeaea");
    });
    $(".item2" ).mouseover(function() {
        $(".evaluation_point2").css("color", "#eaeaea");
    });
    $(".item3" ).mouseover(function() {
        $(".evaluation_point3").css("color", "#eaeaea");
    });
    $(".item4" ).mouseover(function() {
        $(".evaluation_point4").css("color", "#eaeaea");
    });
    $(".item5" ).mouseover(function() {
        $(".evaluation_point5").css("color", "#eaeaea");
    });
    $(".item6" ).mouseover(function() {
        $(".evaluation_point6").css("color", "#eaeaea");
    });
    $(".item7" ).mouseover(function() {
        $(".evaluation_point7").css("color", "#eaeaea");
    });
    $(".item8" ).mouseover(function() {
        $(".evaluation_point8").css("color", "#eaeaea");
    });
    $(".item9" ).mouseover(function() {
        $(".evaluation_point9").css("color", "#eaeaea");
    });
    $(".item10" ).mouseover(function() {
        $(".evaluation_point10").css("color", "#eaeaea");
    });
    $(".item11" ).mouseover(function() {
        $(".evaluation_point11").css("color", "#eaeaea");
    });
    $(".item12" ).mouseover(function() {
        $(".evaluation_point12").css("color", "#eaeaea");
    });
    $(".item13" ).mouseover(function() {
        $(".evaluation_point13").css("color", "#eaeaea");
    });
    $(".item14" ).mouseover(function() {
        $(".evaluation_point14").css("color", "#eaeaea");
    });
    $(".item15" ).mouseover(function() {
        $(".evaluation_point15").css("color", "#eaeaea");
    });

    $(".item1" ).mouseout(function() {
        $(".evaluation_point1").css("color", "#00acee   ");
    });
    $(".item2" ).mouseout(function() {
        $(".evaluation_point2").css("color", "#00acee");
    });
    $(".item3" ).mouseout(function() {
        $(".evaluation_point3").css("color", "#00acee");
    });
    $(".item4" ).mouseout(function() {
        $(".evaluation_point4").css("color", "#00acee");
    });
    $(".item5" ).mouseout(function() {
        $(".evaluation_point5").css("color", "#00acee");
    });
    $(".item6" ).mouseout(function() {
        $(".evaluation_point6").css("color", "#00acee");
    });
    $(".item7" ).mouseout(function() {
        $(".evaluation_point7").css("color", "#00acee");
    });
    $(".item8" ).mouseout(function() {
        $(".evaluation_point8").css("color", "#00acee");
    });
    $(".item9" ).mouseout(function() {
        $(".evaluation_point9").css("color", "#00acee");
    });
    $(".item10" ).mouseout(function() {
        $(".evaluation_point10").css("color", "#00acee");
    });
    $(".item11" ).mouseout(function() {
        $(".evaluation_point11").css("color", "#00acee");
    });
    $(".item12" ).mouseout(function() {
        $(".evaluation_point12").css("color", "#00acee");
    });
    $(".item13" ).mouseout(function() {
        $(".evaluation_point13").css("color", "#00acee");
    });
    $(".item14" ).mouseout(function() {
        $(".evaluation_point14").css("color", "#00acee");
    });
    $(".item15" ).mouseout(function() {
        $(".evaluation_point15").css("color", "#00acee");
    });
</script>
</body>
</html>
